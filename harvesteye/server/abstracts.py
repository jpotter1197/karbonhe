from abc import abstractmethod
from types import MethodType
import socketserver
from queue import Queue, Empty
import threading

from harvesteye import logger

NSLogger = logger.get_logger("Server")
ErrLogger = logger.get_logger("error")


class RequestHandlerAbstract(socketserver.BaseRequestHandler):
    segment_size = 1024

    def __init__(self, *args, **kwargs):
        self._queue = Queue(maxsize=256)
        self._status = False
        super(RequestHandlerAbstract, self).__init__(*args, **kwargs)

    def start(self):
        self.thread = threading.Thread(target=self.read_request, args=())
        self.thread.daemon = True
        self.thread.start()
        return self

    @property
    def message(self):
        """Used to read of the message Queue"""
        try:
            return self._queue.get(timeout=0.5).decode()
        except Empty:
            return None

    def read_request(self):
        """Adds a message to the queue"""
        while self._status:
            try:
                size_info = [None] * 4
                size_info = self.request.recv(4)
                message_length = self.convert_size_from_byte(size_info)
                data = self._receive(int(message_length))
                self._queue.put(data)
            except ConnectionResetError:
                NSLogger.info("Connection reset by peer")
                self._status = False
            except IndexError:
                NSLogger.info("Connection Closed by client")
                self._status = False

    def _receive(self, length):
        """Recieves a messages based on length of bytes"""
        data = [None] * length
        offset = 0

        full_reads = length // self.segment_size
        partial_read = length % self.segment_size
        # perform required number of recieve operations and update offset
        # value in the data array
        # print(datetime.now().time())
        for _ in range(full_reads):
            # if (full_reads - x) % 100 == 0:
            # print("full reads:" + str(full_reads - x))
            data[offset : offset + self.segment_size] = self.request.recv(
                self.segment_size
            ).decode("utf-8")
            offset += self.segment_size
        # perform final partial read
        data[offset : offset + partial_read] = self.request.recv(partial_read).decode(
            "utf-8"
        )
        # print(datetime.now().time())
        data = bytes("".join(data), encoding="utf8")
        NSLogger.info("recieved message: %s", data)
        return data

    def convert_size_from_byte(self, v_size_info):
        """converts 4 byte header into byte size information"""
        messagesize = 0
        messagesize += v_size_info[0]
        messagesize += (v_size_info[1]) << 8
        messagesize += (v_size_info[2]) << 16
        messagesize += (v_size_info[3]) << 24
        return messagesize

    def send_message(self, message):
        """sends a messages with a header of the size information"""
        try:
            NSLogger.info("SENDING MESSAGE BACK TO TABLET")
            size_info = len(message).to_bytes(4, byteorder="big")
            data = size_info + message
            self.request.sendall(data)
        except TypeError:
            NSLogger.info("Message is Nonetype, cannot send.")

    @abstractmethod
    def handle(self):
        pass

    @classmethod
    def attach_handles(cls, handles):
        for key in handles:
            NSLogger.debug("Attaching %s to server", handles)
            cls.attact_handle(key, handles[key])

    @classmethod
    def attact_handle(cls, name, handle):
        NSLogger.debug("Attaching %s to server", handle)
        setattr(cls, name, MethodType(handle, cls))
