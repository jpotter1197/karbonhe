import pickle
import os
import time
from harvesteye import logger

NSLogger = logger.get_logger("Global")
ErrLogger = logger.get_logger("error")
ROOT = os.path.dirname(os.path.join(os.path.abspath(os.path.dirname(__file__))))
ROOT = os.path.join(ROOT, ".globs")
if not os.path.exists(ROOT):
    os.mkdir(ROOT)

print("Reading/Writing Globals from {}".format(ROOT))


class Serializer:
    """Class used to dump data into a pkl"""

    def __init__(self, root):
        self._root = root

    def __call__(self, file_name, data):
        with open(os.path.join(self._root, file_name), "wb") as file:
            pickle.dump(data, file)


class Deserializer:
    """Class used to read data from pkl"""

    def __init__(self, root):
        self._root = root

    def __call__(self, file_name):
        try:
            with open(os.path.join(self._root, file_name), "rb") as file:
                return pickle.load(file)
        except FileNotFoundError:
            return {}


class Globals:
    _defaults = {}

    def __init__(self, name):
        self.__dict__["name"] = name
        self.load()

    def __setattr__(self, name, value):
        NSLogger.debug("Setting Global %s: %s", name, value)
        self.__dict__[name] = value
        Serializer(ROOT)(self.name, self.__dict__)

    def reset(self):
        NSLogger.debug("resetting Global Variables")
        print(os.path.exists(os.path.join(ROOT, self.name)))
        if os.path.exists(os.path.join(ROOT, self.name)):
            os.remove(os.path.join(ROOT, self.name))
        self.__dict__.update(self._defaults)

    def load(self):
        saved = Deserializer(ROOT)(self.name)
        loaded = self._defaults.copy()
        loaded.update((k, saved[k]) for k in set(self._defaults).intersection(saved))
        self.__dict__.update(loaded)
        NSLogger.debug("Loaded Global Variable %s", self.__dict__)

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return str(self.__dict__)


class System(Globals):
    _defaults = {
        "grower": "B-Hive",
        "field": "Test",
        "variety": "test",
        "detection": "PrecisionEdgeDetection",
        "last_capture": time.time(),
        "time_between_capture": 120,
    }


class Calibration(Globals):
    _defaults = {
        "ROI1": [0, 0, 848, 480],
        "ROI2": [0, 0, 0, 0],
        "max_distance": 2,
        "fixed_inc": None,
        "confidence_thresh": 0.1,
        "inference_res": 832,
        "threshold_thresh": 0.80,
    }
