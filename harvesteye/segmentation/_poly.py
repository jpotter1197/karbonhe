"""Module for Poly-Yolo object detection."""
import time
import cv2
import numpy as np
from harvesteye.segmentation.abstracts import Segmenter
from harvesteye.third_party_models import poly_yolo_lite as poly
from harvesteye.segmentation._detections import BboxDetection
from harvesteye import logger

NSLogger = logger.get_logger("Segmentation")
ErrLogger = logger.get_logger("error")


class Poly(Segmenter):
    def setup(self):
        NSLogger.info("POLY Model variables: building")
        self.trained_model = poly.YOLO(
            model_path=self.h5,
            anchors_path=self.anc,
            classes_path=self.names,
            iou=self.threshold_thresh,
            score=self.confidence_thresh,
        )
        NSLogger.info("POLY %s Model variables: built")
        NSLogger.debug("POLY built with:", self.__dict__)

    def inference(self, frame, detection_type=BboxDetection):
        try:
            image = cv2.cvtColor(np.array(frame.masked), cv2.COLOR_BGR2RGB)
            disp = np.array(frame.colour_frame)
            detections = self.detections(frame=frame)
            startx = time.time()
            # boxes are y1, x1, y2, x2
            _detections = self.trained_model.detect_image(image)
            boxes, confidence, classes, polygons = _detections
            NSLogger.debug(
                "POLY: inference speed: ", 1.0 / (time.time() - startx), "fps"
            )
            # need boxes as top x, center y, width, height
            refactored_boxes = []
            for k in range(0, len(boxes)):
                width = boxes[k][3] - boxes[k][1]
                height = boxes[k][2] - boxes[k][0]
                center_x = boxes[k][1] + width / 2
                center_y = boxes[k][0] + height / 2
                refactored_boxes.append([center_x, center_y, width, height])

            for idx in range(1, len(refactored_boxes)):
                detection = detection_type(
                    bbox=refactored_boxes[idx],
                    confidence=float(confidence[idx]),
                    class_id=classes[idx],
                    polygon=polygons[idx],
                    shape=disp.shape,
                    frame=frame,
                )
                detections.append(detection)
            NSLogger.info(
                "POLY: inference speed: %s fps, found %s detections ",
                1.0 / (time.time() - startx),
                len(detections),
            )
            detections = self.filter(detections)
            detections.calculate_dimensions()
            disp = detections.draw_on(disp)
            self.object_queue.put(detections)
            NSLogger.debug("POLY: Added %s Objects to queue", len(detections))
            return disp
        except Exception as error:
            raise error

    def filter(self, objs):
        """Used to filter out unwanted objects."""
        try:
            idxs = cv2.dnn.NMSBoxes(
                [obj.bbox for obj in objs],
                [obj.confidence for obj in objs],
                self.confidence_thresh,
                self.threshold_thresh,
            )
            filtered = self.detections(frame=objs.frame)
            # ensure at least one detection exists
            if len(idxs) > 0:
                # loop over the indexes we are keeping
                for i in idxs.flatten():
                    # extract the bounding box coordinates
                    obj = objs[i]
                    # Remove those that are too near the
                    # borders and those which have a very wrong ratio
                    ratio = max(obj.height, obj.width) / min(obj.height, obj.width)
                    if ratio < 4 and obj.bbox[0] > 0 and obj.bbox[1] > 0:
                        filtered.append(obj)
            NSLogger.debug("%s objects removed by filter", len(objs) - len(filtered))
            return filtered
        except Exception as error:
            NSLogger.exception("POLY: %s Errored while filtering", self.__dict__)
            ErrLogger.error("POLY: Errored while filtering")
            raise error

    @classmethod
    def is_segmenter(cls, model_files):
        required_files = ["anc", "h5", "names"]
        required_files.sort()
        model_files.sort()
        return required_files == model_files
