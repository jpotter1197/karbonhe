import filecmp
import os
import json
import argparse
import tarfile
import itertools
from shutil import copy
from shutil import rmtree
from progress import bar


def get_files(directory, split_point):
    file_list = []
    for dirpath, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            file_list.append(os.path.join(dirpath, filename).split(split_point)[1])
    return file_list


def add_install_location(file_list):
    new_list = []
    for filename in file_list:
        new_filename = args.install_path + filename
        new_list.append(new_filename)
    return new_list


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--current")
    parser.add_argument("--to")
    parser.add_argument("--name", default="app")
    parser.add_argument("--install_path", default="/mnt/data/app/")
    args = parser.parse_args()

    current_build = os.path.join(os.getcwd(), "builds", args.current)
    to_build = os.path.join(os.getcwd(), "builds", args.to)

    current_files = get_files(current_build, f"/builds/{args.current}/")
    to_files = get_files(to_build, f"/builds/{args.to}/")

    updated_list = []
    deleted_list = []
    created_list = []
    print(f"Comparing files between builds {args.current} and {args.to}...")
    for current_file in current_files:
        if current_file in to_files:
            current_file_full_path = f"{os.getcwd()}/builds/{args.current}/{current_file}"
            to_file_full_path = f"{os.getcwd()}/builds/{args.to}/{current_file}"
            if filecmp.cmp(current_file_full_path, to_file_full_path):
                pass
            else:
                updated_list.append(current_file)
        elif current_file not in to_files:
            deleted_list.append(current_file)
    for to_file in to_files:
        if to_file not in current_files:
            created_list.append(to_file)

    # Create patch folder/json name.
    print("Copying over required files...")
    patch_format = "{0}@{1}-{2}".format(args.name, args.current, args.to)
    try:
        os.makedirs(os.path.join(os.getcwd(), "patches", patch_format))
    except FileExistsError:
        rmtree(os.path.join(os.getcwd(), "patches", patch_format))
        os.makedirs(os.path.join(os.getcwd(), "patches", patch_format))

    # Move created and updated files to patch folder.
    for filename in created_list + updated_list:
        filename_no_path = filename.split("/")[-1]
        copy(f"{os.getcwd()}/builds/{args.to}/{filename}", f"{os.getcwd()}/patches/{patch_format}/{filename_no_path}")

    print("Building patch:")
    install_updated_list = add_install_location(updated_list)
    install_deleted_list = add_install_location(deleted_list)
    install_created_list = add_install_location(created_list)

    # Create cmd file
    open(os.path.join(os.getcwd(), "patches", patch_format, "cmd.txt"), "a").close()

    # Create json
    json_dict = {"deleted": install_deleted_list, "created": install_created_list, "updated": install_updated_list,
                 "deleted_dirs": []}
    with open(os.path.join(os.getcwd(), "patches", patch_format, patch_format + ".json"), "wb") as f:
        f.write(json.dumps(json_dict).encode())

    # Create patch .tar
    bar_max = len([name for name in os.listdir(f"{os.getcwd()}/patches/{patch_format}")
                   if os.path.isfile(f"{os.getcwd()}/patches/{patch_format}/{name}")])
    progress_bar = bar.IncrementalBar("Processing", max=bar_max)
    with tarfile.open(os.path.join(os.getcwd(), "patches", patch_format + ".tgz"), "w:gz") as tar:
        for filename in os.listdir(os.path.join(os.getcwd(), "patches", patch_format)):
            tar.add(os.path.join(os.getcwd(), "patches", patch_format, filename), arcname=filename.split("/")[-1])
            progress_bar.next()
    progress_bar.finish()

    # Clean up
    rmtree(os.path.join(os.getcwd(), "patches", patch_format))
