"""unit testing for realsense class"""
import unittest
import time
import os

import cv2
import numpy as np
from harvesteye.camera import Realsense

WORKING_DIR = os.getcwd()


class TestRealSense(unittest.TestCase):
    def setUp(self):
        self.cam = Realsense()

    def tearDown(self):
        self.cam._reset()

    def test_call(self):
        """Testing grabbing a frame from the camera"""
        print("Test Call")
        frames = self.cam()
        self.assertEqual(frames.shape, (self.cam.yres, self.cam.xres, 3))

    def test_borg(self):
        """testing borg"""
        print("Test Borg")
        # create list of cams
        camera_objects = list()
        for i in range(0, 2):
            camera_objects.append(Realsense())
        # check to see if each camrea is reading the same attr
        for cam in camera_objects:
            self.assertEqual(cam.xres, 848)
            self.assertEqual(cam.yres, 480)
        # init a new camera with new attr
        cam = Realsense(xres=1280, yres=720)
        for cam in camera_objects:
            self.assertEqual(cam.xres, 1280)
            self.assertEqual(cam.yres, 720)

    def test_intrinsics(self):
        print("Test Grabbing Intrinsics")
        intrinsics = self.cam.intrinsics
        self.assertEqual(intrinsics.height, 480)
        self.assertEqual(intrinsics.width, 848)
        self.assertEqual(str(intrinsics.model), "distortion.brown_conrady")
        self.cam = Realsense(xres=1280, yres=720)
        intrinsics = self.cam.intrinsics
        self.assertEqual(intrinsics.height, 720)
        self.assertEqual(intrinsics.width, 1280)
        self.assertEqual(str(intrinsics.model), "distortion.brown_conrady")


class TestCompositeFrame(unittest.TestCase):
    def setUp(self):
        cam = Realsense()
        self.frames = cam()

    def tearDown(self):
        pass

    def colour_frame(self):
        print("test colour frame")
        self.assertEqual(self.frames.colour.frame is not None, True)

    def test_get_distance(self):
        print("test get distance")
        self.assertEqual(self.frames.get_distance((0, 100), (0, 100)) >= 0, True)

    def test_set_mask(self):
        print("test setting mask")
        mask = np.full((480, 848), False, dtype=bool)
        key = [0, 0, 848, 480]
        mask[key[1] : key[3], key[0] : key[2]] = True
        self.frames.set_mask(key)
        np.testing.assert_array_equal(self.frames._mask, mask)

    def test_shape(self):
        print("test grabbing shape")
        self.assertEqual(self.frames.shape[0], 480)
        self.assertEqual(self.frames.shape[1], 848)
        self.assertEqual(self.frames.shape[2], 3)
        pass

    def test_masked(self):
        print("test masked")
        colour = self.frames.colour_frame
        mask = np.full((480, 848), False, dtype=bool)
        key = [0, 0, 848, 480]
        mask[key[1] : key[3], key[0] : key[2]] = True
        self.frames.set_mask(key)
        masked = cv2.bitwise_or(colour, colour, mask=1 * mask.astype(np.uint8))
        np.testing.assert_array_equal(self.frames.masked, masked)

    def test_cropped_masked(self):
        print("test cropped masked")
        colour = self.frames.colour_frame
        mask = np.full((480, 848), False, dtype=bool)
        key = [0, 0, 848, 480]
        mask[key[1] : key[3], key[0] : key[2]] = True
        self.frames.set_mask(key)
        masked = cv2.bitwise_or(colour, colour, mask=1 * mask.astype(np.uint8))
        np.testing.assert_array_equal(
            self.frames.cropped_masked, masked[key[1] : key[3], key[0] : key[2]]
        )


if __name__ == "__main__":
    WORKING_DIR = "/home/b-hive/bitbucket/harvesteye/"
    unittest.main()
