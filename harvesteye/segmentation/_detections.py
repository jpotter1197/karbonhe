"""Model to calculate object sizes from a variety of detection methods."""
import operator
import copy
import imutils
import cv2
import numpy as np
from harvesteye.segmentation.abstracts import Detection, Detections
from harvesteye.camera import FrameFromFile
from harvesteye import logger

NSLogger = logger.get_logger("Segmentation")
ErrLogger = logger.get_logger("error")


class BboxDetection(Detection):
    """
    attr: height, width, center
    """

    def __init__(self, bbox, confidence, class_id, shape=(480, 848), **kwargs):
        args = {"bbox": bbox, "conf": confidence, "class": class_id, "shape": shape}
        try:
            super().__init__(kwargs["id"])
            self.center_x, self.center_y, self.width, self.height = bbox
            self.confidence = confidence
            self.class_id = class_id
            self.scale = 1
            self.shape = shape
            self.filtered = False
            NSLogger.debug("detection %s: %s Created", self.ID, args)
        except Exception as error:
            NSLogger.exception(
                "detection %s: %s Errored while initalising %s", self.ID, args, error
            )
            ErrLogger.error(
                "detection %s: Errored while initalising: %s", self.ID, error
            )
            raise error

    @property
    def major(self):
        return self._calc(operator.ge)

    @property
    def minor(self):
        return self._calc(operator.le)

    @property
    def bbox(self):
        """Converts format of bounding box."""
        try:
            if hasattr(self, "_bbox"):
                return self._bbox
            bbox = [
                int(self.center_x - (self.width / 2) * self.scale),
                int(self.center_y - (self.height / 2) * self.scale),
                int(self.center_x + (self.width / 2) * self.scale),
                int(self.center_y + (self.height / 2) * self.scale),
            ]
            if bbox[0] < 0:
                bbox[0] = 0
            if bbox[1] < 0:
                bbox[1] = 0
            if bbox[2] >= self.shape[1]:
                bbox[2] = self.shape[1] - 1
            if bbox[3] >= self.shape[0]:
                bbox[3] = self.shape[0] - 1
            self._bbox = bbox
            self.width = bbox[2] - bbox[0]
            self.height = bbox[3] - bbox[1]
            return bbox
        except Exception as error:
            NSLogger.exception(
                "detection %s: %s Errored while calling bbox property: %s",
                self.ID,
                self.__dict__,
                error,
            )
            ErrLogger.error(
                "detection %s: Errored while calling bbox property: %s", self.ID, error
            )
            raise error

    @property
    def center(self):
        """Returns the center co-ords of the bounding box."""
        ret = (int(self.center_x), int(self.center_y))
        return ret

    def _calc(self, operation):
        if operation(self.height, self.width):
            return (self.bbox[0], self.bbox[1]), (self.bbox[0], self.bbox[3])
        else:
            return (self.bbox[0], self.bbox[1]), (self.bbox[2], self.bbox[1])

    @property
    def bbox_area(self):
        return self.width * self.height

    def draw(self, display):
        try:
            if self.color is None:
                self.color = [int(c) for c in self.colors[self.class_id]]
            cv2.rectangle(
                display,
                (self.bbox[0], self.bbox[1]),
                (self.bbox[2], self.bbox[3]),
                tuple(self.color),
                2,
            )
            if NSLogger.level == 10:
                cv2.putText(
                    display,
                    str(self.ID),
                    self.center,
                    cv2.FONT_HERSHEY_SIMPLEX,
                    0.5,
                    (255, 255, 255),
                    2,
                )
            NSLogger.debug(
                "detection %s bbox %s drawn on input image", self.ID, self.bbox
            )
            return display
        except Exception as error:
            NSLogger.exception(
                "detection %s: %s Errored while drawing bbox: %s",
                self.ID,
                self.__dict__,
                error,
            )
            ErrLogger.error(
                "detection %s: Errored while drawing bbox: %s", self.ID, error
            )
            raise error

    def keep(self, regions):  # noqa: 901
        near_regions = [not self.near_region(region) for region in regions]
        ratio = max(self.height, self.width) / min(self.height, self.width)
        if self.width < 5:
            NSLogger.debug(
                "Failed to calulate %s width being too small %s", self.ID, self.width
            )
            if NSLogger.level == 10:
                self.color = (0, 40, 255)
            return False
        if self.height < 5:
            NSLogger.debug(
                "Failed to calulate %s height being too small %s", self.ID, self.height
            )
            if NSLogger.level == 10:
                self.color = (0, 40, 255)
            return False

        if not any(near_regions):
            NSLogger.debug("Failed to size %s , object too close to edge", self.ID)
            if NSLogger.level == 10:
                self.color = (0, 70, 255)
            return False

        if not ratio < 4:
            NSLogger.debug(
                "Failed to size %s due to: ratio %s(height%s/width %s) > 4",
                self.ID,
                self.height,
                self.width,
                self.height / self.width,
            )
            if NSLogger.level == 10:
                self.color = (0, 210, 180)
            return False

        if self.dimensions is not None:
            if None in self.dimensions[0:2]:
                NSLogger.debug(
                    "Failed to calulate %s size due to None return %s",
                    self.ID,
                    self.dimensions[0:2],
                )
                if NSLogger.level == 10:
                    self.color = (0, 0, 255)
                return False

            for size in self.dimensions:
                if size >= 180:
                    NSLogger.debug(
                        "Failed to size %s , return size to big %s",
                        self.ID,
                        self.dimensions[0:3],
                    )
                    if NSLogger.level == 10:
                        self.color = (0, 140, 220)
                    return False

            for size in self.dimensions:
                if size <= 10:
                    NSLogger.debug(
                        "Failed to size %s, return size too small %s",
                        self.ID,
                        self.dimensions[0:3],
                    )
                    if NSLogger.level == 10:
                        self.color = (0, 140, 220)
                    return False
            # for size in self.dimensions:
            #     if size == "nan":
            #         print("nan")
            #         return False

        if NSLogger.level == 10:
            self.color = (0, 255, 0)
        return True


class EdgeDetection(BboxDetection):
    """Adds edge detection to yolo bounding box to get a mask of the tuber.
    Should provide more accurate sizing."""

    PADDING = 0

    def __init__(self, frame, **kwargs):
        try:
            super().__init__(**kwargs)
            self._selection = frame.crop(self.bbox, self.PADDING)
            self._top = None
            self._bottom = None
            self._left = None
            self._right = None
            self.ellipse = None
            self.ellipse_colour = (255, 255, 0)
            self._trig_calculations()
        except cv2.error:
            raise ValueError("5 points to fit the ellipse")

    def _process_frame(self):
        """Performs edge detection on section of frame within bounding box.
        Fits an elipse to the contour with the largest area."""
        try:
            _gray = cv2.cvtColor(self._selection, cv2.COLOR_BGR2GRAY)
            _blurred = cv2.GaussianBlur(_gray, (3, 3), 0)
            # _, _edges = cv2.threshold(_blurred,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
            _edges = cv2.adaptiveThreshold(
                _blurred, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 1
            )
            _contours = cv2.findContours(_edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            _contours = imutils.grab_contours(_contours)
            # print(len(_contours))
            _largest_contour = max(_contours, key=cv2.contourArea)
            self.ellipse = cv2.fitEllipse(_largest_contour)
            NSLogger.debug(
                "EdgeDetection %s: %s Ellipse Fitted:", self.ID, self.ellipse
            )
            return self.ellipse
        except cv2.error as error:
            raise error
        except Exception as error:
            NSLogger.exception(
                "EdgeDetection %s: %s Errored while processing frame: %s",
                self.ID,
                self.__dict__,
                error,
            )
            ErrLogger.error(
                "EdgeDetection %s: Errored while processing frame: %s", self.ID, error
            )
            raise error

    def _trig_calculations(self):
        """Trig calculations to convert ellipse dimensions to co-ords at the top,
        bottom, left and right edge of the tuber."""
        try:
            ellipse = self._process_frame()
            # Center co-ords [x,y]: Ellipse[0]
            # Width, length [w, l]: Ellipse[1]
            # Angle of ellipse: Ellipse[2]
            # Ellipse pointing left.
            self.ellipse_area = np.pi * ellipse[1][0] / 2 * ellipse[1][1] / 2
            self.orig_angle = ellipse[2]
            if ellipse[2] > 90:
                _angle = np.deg2rad(180 - ellipse[2])
            # Ellipse pointing right.
            if ellipse[2] <= 90:
                _angle = np.deg2rad(ellipse[2])

            # print(ellipse[2], self.ID)
            # Horizontal and verticle components of length and width.
            _h_length = np.sin(_angle) * (ellipse[1][1] / 2)
            _v_length = np.cos(_angle) * (ellipse[1][1] / 2)
            _h_width = np.cos(_angle) * (ellipse[1][0] / 2)
            _v_width = np.sin(_angle) * (ellipse[1][0] / 2)
            # Calcualte co-ords for top, bottom, left, right of ellipse
            _top_y = ellipse[0][1] - _v_length
            _bottom_y = ellipse[0][1] + _v_length
            _left_x = ellipse[0][0] - _h_width
            _right_x = ellipse[0][0] + _h_width
            # Top_x, bottom_x, left_y, right_y change based on angle of ellipse.
            if ellipse[2] > 90:
                _h_length *= -1.0
                _v_width *= -1.0
            _top_x = ellipse[0][0] + _h_length
            _bottom_x = ellipse[0][0] - _h_length
            _left_y = ellipse[0][1] - _v_width
            _right_y = ellipse[0][1] + _v_width
            # Return co-ords for top, bottom, left, right of ellipse.
            self._top = (_top_x, _top_y)
            self._bottom = (_bottom_x, _bottom_y)
            self._left = (_left_x, _left_y)
            self._right = (_right_x, _right_y)
            # totally readable do judge.
            if any(
                tuple(map(operator.add, x, (self.bbox[0], self.bbox[1]))) < (0, 0)
                for x in [self._top, self._bottom, self._left, self._right]
            ):
                raise ValueError
            if any(
                tuple(map(operator.add, x, (self.bbox[0], self.bbox[1])))
                > self.shape[0:2][::-1]
                for x in [self._top, self._bottom, self._left, self._right]
            ):
                raise ValueError
            NSLogger.debug(
                "EdgeDetection %s: %s trigs calulcated",
                self.ID,
                {
                    "top": self._top,
                    "bottom": self._bottom,
                    "left": self._left,
                    "right": self._right,
                },
            )
        except cv2.error as error:
            raise error

    @property
    def major(self):
        """Returns pixel co-ords for the top and bottom of the major axis
        of the ellipse fitted to the masked tuber."""
        # Convert co-ords back from mask to full frame.
        try:
            _x_offset = self.bbox[0]
            _y_offset = self.bbox[1]

            # pointing left
            if self.orig_angle > 90:
                major_top = (
                    int(np.floor(self._top[0]) + _x_offset),
                    int(np.floor(self._top[1]) + _y_offset),
                )
                major_bottom = (
                    int(np.ceil(self._bottom[0]) + _x_offset),
                    int(np.ceil(self._bottom[1]) + _y_offset),
                )
            # pointing right
            elif self.orig_angle <= 90:
                major_top = (
                    int(np.ceil(self._top[0]) + _x_offset),
                    int(np.floor(self._top[1]) + _y_offset),
                )
                major_bottom = (
                    int(np.floor(self._bottom[0]) + _x_offset),
                    int(np.ceil(self._bottom[1]) + _y_offset),
                )

            NSLogger.debug(
                "EdgeDetection %s major: %s property call",
                self.ID,
                (major_top, major_bottom),
            )
            return (major_top, major_bottom)
        except ValueError:
            if "cannot convert float NaN to integer" in str(ValueError):
                ((0, 0), (0, 0))
        except Exception as error:
            NSLogger.exception(
                "EdgeDetection %s: %s Errored while calling major %s",
                self.ID,
                self.__dict__,
                error,
            )
            ErrLogger.error(
                "EdgeDetection %s: Errored while calling major: s%", self.ID, error
            )
            raise error

    @property
    def minor(self):
        """Returns pixel co-ords for the top and bottom of the minor axis
        of the ellipse fitted to the masked tuber."""
        try:
            # Convert co-ords back from mask to full frame.
            _x_offset = self.bbox[0]
            _y_offset = self.bbox[1]

            # pointing left
            if self.orig_angle > 90:
                minor_bottom = (
                    int(np.ceil(self._left[0]) + _x_offset),
                    int(np.floor(self._left[1]) + _y_offset),
                )
                minor_top = (
                    int(np.floor(self._right[0]) + _x_offset),
                    int(np.ceil(self._right[1]) + _y_offset),
                )
            # pointing right
            elif self.orig_angle <= 90:
                minor_bottom = (
                    int(np.ceil(self._left[0]) + _x_offset),
                    int(np.ceil(self._left[1]) + _y_offset),
                )
                minor_top = (
                    int(np.floor(self._right[0]) + _x_offset),
                    int(np.floor(self._right[1]) + _y_offset),
                )

            NSLogger.debug(
                "EdgeDetection %s minor: %s property call",
                self.ID,
                (minor_top, minor_bottom),
            )
            return (minor_top, minor_bottom)
        except ValueError:
            if "cannot convert float NaN to integer" in str(ValueError):
                return ((0, 0), (0, 0))
        except Exception as error:
            NSLogger.exception(
                "EdgeDetection %s: %s Errored while calling minor %s",
                self.ID,
                self.__dict__,
                error,
            )
            ErrLogger.error("EdgeDetection %s: Errored while calling minor", self.ID)
            raise error

    @property
    def ellipse_coords(self):
        """Returns co-ords of ellipse for use in draw on function."""
        try:
            _x_offset = self.bbox[0]
            _y_offset = self.bbox[1]
            _x_ellipse = self.ellipse[0][0] + _x_offset
            _y_ellipse = self.ellipse[0][1] + _y_offset
            _offset_ellipse = (
                (_x_ellipse, _y_ellipse),
                self.ellipse[1],
                self.ellipse[2],
            )
            NSLogger.debug(
                "EdgeDetection %s ellipse_coords: %s property call",
                self.ID,
                _offset_ellipse,
            )
            return _offset_ellipse
        except Exception as error:
            NSLogger.exception(
                "EdgeDetection %s: %s Errored while calling ellipse_coords: %s",
                self.ID,
                self.__dict__,
                error,
            )
            ErrLogger.error(
                "EdgeDetection %s: Errored while calling ellipse_coords", self.ID
            )
            raise error

    @property
    def center(self):
        """Returns the center co-ords of the bounding ellipse."""
        ret = (
            int(self.bbox[0] + self.ellipse[0][0]),
            int(self.bbox[1] + self.ellipse[0][1]),
        )
        NSLogger.debug("EdgeDetection %s center: %s property call", self.ID, ret)
        return ret

    def draw(self, display):
        try:
            if NSLogger.level == 10:
                display = super().draw(display)
            ellipse = self.ellipse_coords
            cv2.ellipse(display, ellipse, self.ellipse_colour, 2)
            NSLogger.debug(
                "EdgeDetection %s ellipse %s drawn on input image", self.ID, ellipse
            )
            return display
        except Exception as error:
            NSLogger.exception(
                "EdgeDetection %s: %s Errored while drawing bbox: %s",
                self.ID,
                self.__dict__,
                error,
            )
            ErrLogger.error("EdgeDetection %s: Errored while drawing bbox", self.ID)
            raise error

    def keep(self, regions):
        ellipse_length = np.sqrt(
            (self.major[0][0] - self.major[1][0]) ** 2
            + (self.major[0][1] - self.major[1][1]) ** 2
        )
        ellipse_width = np.sqrt(
            (self.minor[0][0] - self.minor[1][0]) ** 2
            + (self.minor[0][1] - self.minor[1][1]) ** 2
        )
        ratio = ellipse_length / ellipse_width

        # IDEA: to determine if ellipses are being put on a potato more than once; get center point, check against list of other center points,
        # check how close it is to the next nearest center point, if that center is closer than the pseudo radius (im thick leave it out) ellipse width / 2
        # then it is too close and it must be fit on the same potato?

        if not super().keep(regions):
            return False

        if ratio >= 4:
            NSLogger.debug(
                "Failed to size %s due to Ellipse Ratio, %sRatio > 4",
                self.ID,
                ratio,
            )
            return False

        if self.bbox_area / self.ellipse_area > 2.2:
            NSLogger.debug(
                "Failed to size %s due to Area , %s(box %s/ellipse %s) > 2.2",
                self.ID,
                self.bbox_area / self.ellipse_area,
                self.bbox_area,
                self.ellipse_area,
            )
            if NSLogger.level == 10:
                self.color = (0, 180, 220)
                self.ellipse_colour = (0, 0, 255)
            return False

        if self.bbox_area < self.ellipse_area:
            NSLogger.debug(
                "Failed to size %s due to Area , box %s >ellipse %s",
                self.ID,
                self.bbox_area,
                self.ellipse_area,
            )
            if NSLogger.level == 10:
                self.color = (0, 180, 220)
                self.ellipse_colour = (0, 0, 255)
            return False

        return True


class PrecisionEdgeDetection(EdgeDetection):
    """Sub-pixel measuring."""

    @property
    def major(self):
        """Returns pixel co-ords for the top and bottom of the major axis
        of the ellipse fitted to the masked tuber."""
        # Convert co-ords back from mask to full frame.
        try:
            _x_offset = self.bbox[0]
            _y_offset = self.bbox[1]

            major_top = (self._top[0] + _x_offset, self._top[1] + _y_offset)
            major_bottom = (self._bottom[0] + _x_offset, self._bottom[1] + _y_offset)

            NSLogger.debug(
                "EdgeDetection %s major: %s property call",
                self.ID,
                (major_top, major_bottom),
            )
            return (major_top, major_bottom, self.orig_angle)
        except Exception as error:
            NSLogger.exception(
                "EdgeDetection %s: %s Errored while calling major: %s",
                self.ID,
                self.__dict__,
                error,
            )
            ErrLogger.error("EdgeDetection %s: Errored while calling major", self.ID)
            raise error

    @property
    def minor(self):
        """Returns pixel co-ords for the top and bottom of the minor axis
        of the ellipse fitted to the masked tuber."""
        try:
            # Convert co-ords back from mask to full frame.
            _x_offset = self.bbox[0]
            _y_offset = self.bbox[1]

            if self.orig_angle <= 90:
                minor_top = (self._right[0] + _x_offset, self._right[1] + _y_offset)
                minor_bottom = (self._left[0] + _x_offset, self._left[1] + _y_offset)
            elif self.orig_angle > 90:
                minor_top = (self._left[0] + _x_offset, self._left[1] + _y_offset)
                minor_bottom = (self._right[0] + _x_offset, self._right[1] + _y_offset)
            else:
                ErrLogger.error(
                    "Can't find a minor: ID: %s, angle:%s", self.ID, self.orig_angle
                )
                NSLogger.debug(
                    "Can't find a minor: ID: %s, angle:%s", self.ID, self.orig_angle
                )
            NSLogger.debug(
                "EdgeDetection %s minor: %s property call",
                self.ID,
                (minor_top, minor_bottom),
            )
            return (minor_top, minor_bottom, self.orig_angle)
        except Exception as error:
            NSLogger.exception(
                "EdgeDetection %s: %s Errored while calling minor: %s",
                self.ID,
                self.__dict__,
                error,
            )
            ErrLogger.error(
                "EdgeDetection %s: Errored while calling minor %s", self.ID, error
            )
            raise error


class PolyDetection(EdgeDetection):
    """Polygon detection used for Poly-Yolo."""

    def __init__(self, polygon, **kwargs):
        try:
            self.polygon = polygon
            self.points = None
            super().__init__(**kwargs)
        except Exception as error:
            NSLogger.exception(
                "PolyDetection %s: %s Errored while initalising", self.ID, kwargs
            )
            ErrLogger.error("PolyDetection %s: Errored while initalising", self.ID)
            raise error

    def _process_frame(self):
        """Fits ellipse to polygon of object."""
        try:
            offset = len(self.polygon) // 3
            self.points = []
            for dst in range(0, len(self.polygon) // 3):
                if self.polygon[dst + offset * 2] > 0.3:
                    self.points.append(
                        [int(self.polygon[dst]), int(self.polygon[dst + offset])]
                    )
            self.points = np.asarray(self.points)
            self.ellipse = cv2.fitEllipse(self.points)
            NSLogger.debug(
                "PolyDetection %s: %s ellipse calulated", self.ID, self.ellipse
            )
            return self.ellipse
        except Exception as error:
            NSLogger.exception(
                "PolyDetection %s: %s Errored while processing", self.ID, self.__dict__
            )
            ErrLogger.error("PolyDetection %s: Errored while processing", self.ID)
            raise error

    @property
    def major(self):
        """Returns pixel co-ords for the top and bottom of the major axis
        of the ellipse fitted to the masked tuber."""
        # Round to whole number for pixel coord.
        try:
            major_top = (int(round(self._top[0])), int(round(self._top[1])))
            major_bottom = (int(round(self._bottom[0])), int(round(self._bottom[1])))
            NSLogger.debug(
                "PolyDetection %s major: %s property call",
                self.ID,
                (major_top, major_bottom),
            )
            return (major_top, major_bottom)
        except Exception as error:
            NSLogger.exception(
                "PolyDetection  %s major: %s Errored calling property",
                self.ID,
                self.__dict__,
            )
            ErrLogger.error("PolyDetection %s major: Errored calling property", self.ID)
            raise error

    @property
    def minor(self):
        """Returns pixel co-ords for the top and bottom of the minor axis
        of the ellipse fitted to the masked tuber."""
        # Round to whole number for pixel coord.
        try:
            minor_top = (int(round(self._left[0])), int(round(self._left[1])))
            minor_bottom = (int(round(self._right[0])), int(round(self._right[1])))
            NSLogger.debug(
                "PolyDetection %s minor: %s property call",
                self.ID,
                (minor_top, minor_bottom),
            )
            return (minor_top, minor_bottom)
        except Exception as error:
            NSLogger.exception(
                "PolyDetection %s minor: %s Errored calling property: s%",
                self.ID,
                self.__dict__,
                error,
            )
            ErrLogger.error(
                "PolyDetection %s minor: Errored calling property: %s", self.ID, error
            )
            raise error

    @property
    def ellipse_coords(self):
        return self.ellipse

    @property
    def center(self):
        """Returns the center co-ords of the bounding ellipse."""
        ret = (int(self.ellipse[0][0]), int(self.ellipse[0][1]))
        NSLogger.debug("PolyDetection %s center: %s property call", self.ID, ret)
        return ret

    @property
    def points_to_draw(self):
        """Returns polygon points for use in drawing polygon masks."""
        return [self.points]

    def draw(self, display):
        try:
            display = super().draw(display)
            overlay = copy.copy(display)
            cv2.fillPoly(overlay, self.points_to_draw, (240, 50, 230))
            NSLogger.info(
                "PolyDetection %s: poly %s drawn on input image", self.ID, self.ellipse
            )
            return cv2.addWeighted(overlay, 0.2, display, 1 - 0.2, 0)
        except Exception as error:
            NSLogger.exception(
                "PolyDetection %s: %s Errored while drawing: %s",
                self.ID,
                self.__dict__,
                error,
            )
            ErrLogger.error(
                "PolyDetection %s: Errored while drawing %s", self.ID, error
            )
            raise error


class DetectionsML(Detections):
    HEIGHT_MODEL = None
    SIZEBAND_MODEL = None
    WEIGHT_MODEL = None
    CALCULATION_CLASS = PrecisionEdgeDetection

    def __init__(self, frame):
        super().__init__(frame=frame)
        if any([self.HEIGHT_MODEL, self.SIZEBAND_MODEL, self.WEIGHT_MODEL]) is None:
            raise ValueError("A sizing Model is missing from class")
        self.total_weight = 0

    def calculate_dimensions(self):
        NSLogger.debug("Calculating Sizes for %s detections", len(self))

        for detection in self:
            while not detection.filtered:
                try:
                    detection.dimensions = (
                        self.calc_length(detection),
                        self.calc_width(detection),
                    )
                    if detection.dimensions[0] <= 20 or detection.dimensions[1] <= 20:
                        detection.dimensions = (np.nan, np.nan)
                    if detection.dimensions[0] >= 200 or detection.dimensions[1] >= 200:
                        detection.dimensions = (np.nan, np.nan)

                    if None in detection.dimensions:
                        raise Exception
                    detection.filtered = any(
                        [
                            detection.keep(regions=self.frame.regions),
                            NSLogger.level == 10,
                            isinstance(self.frame, FrameFromFile),
                        ]
                    )
                    if not detection.filtered:
                        detection.__class__ = detection.__class__.__bases__[0]
                        NSLogger.debug("Unsuccessfully Calculated %s", detection.ID)
                    else:
                        NSLogger.debug(
                            "Successfully Calculated %s size: %s",
                            detection.ID,
                            detection.dimensions,
                        )
                except Exception:
                    detection.dimensions = (np.nan, np.nan)
                    break

        lw = [list(d.dimensions) for d in self]
        # print(lw)
        if lw:
            heights = self.HEIGHT_MODEL(np.asarray(lw))
            sizebands = self.SIZEBAND_MODEL(np.asarray(lw))
            weights = self.WEIGHT_MODEL(np.asarray(lw))
            dims = np.c_[
                np.asarray(lw),
                np.asarray(heights),
                np.asarray(sizebands),
                np.asarray(weights),
            ]
            # print(dims)
            for i, dim in enumerate(dims):
                if not np.isnan(dim).any():
                    self[i].dimensions = dim
                    if self.CALCULATION_CLASS not in self[i].__class__.__bases__:
                        self[i].dimensions = (len(dim) + 2) * [0]
                        self[i].dimensions[4] = dim[4]
