import os
from functools import partial
import threading
import numpy as np
from queue import Empty
import cv2
import re
import time
from harvesteye import logger
import signal
from harvesteye.segmentation import attach_gps_to_detection, detection_factory
import harvesteye

NSLogger = logger.get_logger("Server")
ErrLogger = logger.get_logger("error")
BASE_PATH = os.path.dirname(
    os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
)


def test(self, data, string=None):
    return string.encode()


def set_calibration(self, data, objects, globals):
    NSLogger.debug("setting calibration: %s", data)
    try:
        serial = data.attrib["Serial"]
    except KeyError:
        serial = [serial for serial in objects][0]

    set = partial(
        _set, objects=objects[serial].values(), globals=globals, serial=serial
    )

    set("max_distance", int(data.attrib["Height"]) / 1000)

    for roi in ["ROI1", "ROI2"]:
        vals = list(map(int, data.attrib[roi].split(",")))
        set(roi, [vals[1], vals[0], vals[1] + vals[2], vals[0] + vals[3]])

    set("confidence_thresh", int(data.attrib["Confidence"]) / 100)
    set("time_between_capture", int(data.attrib["TimeBetween"]))
    if int(data.attrib["FixedInc"]) == 0:
        fixed_inc = None
    else:
        fixed_inc = int(data.attrib["FixedInc"])
    set("fixed_inc", fixed_inc)

    set("inference_res", int(data.attrib["Inference"]))

    if data.attrib["BBox"] == "True":
        set("detection", "BboxDetection")
    elif data.attrib["BBox"] == "False":
        set("detection", "PrecisionEdgeDetection")

    return "".join("1").encode()


def get_calibration(self, data, globals):
    """get settings"""
    try:
        serial = data.attrib["Serial"]
    except AttributeError:
        serial = [key for key in globals.keys()][1]
    try:
        fixed_inc = None
        if globals[serial].fixed_inc:
            fixed_inc = globals[serial].fixed_inc
        else:
            fixed_inc = 0

        detection = None
        if globals["system"].detection == "BboxDetection":
            detection = int(bool(True))
        else:
            detection = int(bool(False))

        data = np.array(
            [
                globals[serial].ROI1[0],
                globals[serial].ROI1[1],
                globals[serial].ROI1[2] - globals[serial].ROI1[0],
                globals[serial].ROI1[3] - globals[serial].ROI1[1],
                globals[serial].ROI2[0],
                globals[serial].ROI2[1],
                globals[serial].ROI2[2] - globals[serial].ROI2[0],
                globals[serial].ROI2[3] - globals[serial].ROI2[1],
                globals[serial].max_distance * 1000,
                globals["system"].time_between_capture,
                globals[serial].inference_res,
                globals[serial].confidence_thresh * 100,
                fixed_inc,
                int(bool(False)),
                detection,
            ]
        )
        data = data.astype(int)
        data = data.astype(str)
        string_data = "/".join(data).encode()
        NSLogger.debug("getting settings: %s", data)
        return string_data
    except Exception as error:
        raise error


def get_serials(self, data, serials):
    string_data = "/".join(serials).encode()
    return string_data


def set_metadata(self, data, globals):
    """save metadata"""
    try:
        globals.grower = re.sub("[^a-zA-Z.-]+", "-", str(data.attrib["Grower"]))
        globals.field = re.sub("[^a-zA-Z.-]+", "-", str(data.attrib["Field"]))
        globals.variety = re.sub("[^a-zA-Z.-]+", "-", str(data.attrib["Variety"]))
        NSLogger.debug(
            "Saving metadata: %s %s %s", globals.grower, globals.field, globals.variety
        )
        return "".join("1").encode()
    except Exception as error:
        raise error


def _set(name, value, objects, globals, serial):
    for object in objects:
        if hasattr(object, name):
            setattr(object, name, value)
    if hasattr(globals[serial], name):
        setattr(globals[serial], name, value)
    elif hasattr(globals["system"], name):
        setattr(globals["system"], name, value)


def start_threads(self, data, funcs, threads):
    try:
        for func in funcs:
            thread = LoopThread(func)
            threads.append(thread)
        for thread in threads:
            thread.start()
        return "".join("initialised").encode()
    except RuntimeError:
        NSLogger.info("Handled Error, Threads can only be started once")
        return "".join("-1").encode()


def stop(self, data, threads):
    for thread in threads:
        thread.stop()
    threads.clear()
    return "".join("1").encode()


def get_inference(self, data, objs):
    try:
        serial = data.attrib["Serial"]
    except AttributeError:
        serial = [serial for serial in objs][0]
    displays = objs[serial]["displays"]
    """gets labelled frame from frame porcessor"""
    try:

        if displays["inference"] is None:
            display = np.zeros((480, 848, 1), np.int16)
        else:
            display = displays["inference"]
        _, imgencode = cv2.imencode(".jpg", display, [int(1), 50])
        return np.array(imgencode).tostring()
    except Exception as error:
        raise error


def get_colour(self, data, cameras):
    """capture a rbg frame"""
    try:
        for camera in cameras:
            try:
                if not camera.serial == data.attrib["Serial"]:
                    continue
            except AttributeError:
                pass
            frame = camera()
            _, imgencode = cv2.imencode(".jpg", frame.colour_frame, [int(1), 50])
            return np.array(imgencode).tostring()
    except Exception as error:
        NSLogger.error(error, exc_info=True)
        raise error


def clear_file(self, data, files, globals):
    """deletes log files and remakes them"""
    try:
        for file in files:
            with open(file, "w") as _:
                pass
        return "".join("1").encode()
    except Exception as error:
        raise error


def send_patch(self, data, SOFTWARES):
    """save file"""
    try:
        patch_as_bytes = bytearray.fromhex(data.attrib["patch"].replace("-", ""))
        filename = data.attrib["name"]
        name = filename.split("@")
        with open(os.path.join(SOFTWARES.patchpath, filename), "w+b") as file:
            file.write(patch_as_bytes)
            file.close()
        models = ["Potato", "Onion"]
        if name in models:
            models.remove(name)
            for model in models:
                print(model)
                SOFTWARES.uninstall_software(model)
        return "".join("1").encode()
    except Exception as error:
        raise error


def get_version(self, data, SOFTWARES):
    """get version of software"""
    try:
        model_name = data.attrib["crop_type"]
        SOFTWARES.load_software()
        software_list = [model_name, "app", "pytele"]
        versions = []
        for name in software_list:
            if SOFTWARES.is_installed(name):
                software = SOFTWARES[SOFTWARES.idx_of(name)]
                version = str(software.version)
            else:
                version = "-1"
            versions.append(version)
        versions[1] = str(harvesteye.__version__)
        return "/".join(versions).encode()
    except Exception as error:
        raise error


def get_detection_data(self, data, objs):
    try:
        serial = data.attrib["Serial"]
    except AttributeError:
        serial = [serial for serial in objs][0]
    ret = ""
    detections = objs[serial]["segmenter"](1)
    if detections:
        ret += str(detections) + ";"
    if ret != "":
        return ret.encode()
    return "".join("-1").encode()


# should be used with loopthreads
def tracking(objs, globals):
    """This is constantly fed frames from the camera to the Tracker"""
    try:

        frames = objs["camera"]()
        if globals[objs["camera"].serial] and frames:
            frames.set_mask(globals[objs["camera"].serial].ROI1)
            if globals[objs["camera"].serial].ROI2 != [0, 0, 0, 0]:
                frames.set_mask(globals[objs["camera"].serial].ROI2)
            objs["displays"]["tracker"] = objs["tracker"].update(frames)

    except Exception as error:
        print(error)
        os.kill(os.getpid(), signal.SIGKILL)


def captured(objs, GPS, globals):
    """This will grab any 'captured frames' from the tracker"""
    try:
        frames = objs["tracker"](20)
        frame_capture(frames, globals["system"])
        detection = attach_gps_to_detection(
            GPS, detection_factory(globals["system"].detection)
        )
        objs["displays"]["inference"] = objs["segmenter"].inference(frames, detection)
        objs["displays"]["captured"] = frames.colour_frame
    except Empty:
        pass
    except Exception as error:
        print(error)
        os.kill(os.getpid(), signal.SIGKILL)


def frame_capture(frames, globals):
    try:
        directory = os.path.join(BASE_PATH, "frames", globals.variety) + "/"
        if not os.path.isdir(directory):
            os.makedirs(directory)
        listdir = os.listdir(directory)
        if (
            len(listdir) < 250
            and time.time() - globals.last_capture > globals.time_between_capture
        ):
            NSLogger.debug("saving image out to : %s", directory)
            frames.save_colour(
                directory,
                "-".join(
                    [
                        globals.grower,
                        globals.field,
                        globals.variety,
                        time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime(time.time())),
                    ]
                ),
            )
            globals.last_capture = time.time()
        frames.save_colour(BASE_PATH, "/last_frame")
    except Exception as error:
        NSLogger.exception("Error occured during Handle")
        ErrLogger.error("Error occured during Handle")
        raise error


class LoopThread(threading.Thread):
    def __init__(self, func):
        threading.Thread.__init__(self)
        self.running = True
        self.func = func

    def run(self):
        while self.running:
            self.func()

    def stop(self):
        self.running = False
