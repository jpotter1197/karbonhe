"""Main __init for package"""
from threading import Thread
import time
import os
import signal

import harvesteye.camera
import harvesteye.tracker
import harvesteye.segmentation
import harvesteye.globals

from harvesteye import logger
import harvesteye.ftp_server
import harvesteye.harvesteye_server
import harvesteye.reboot_server
import argparse

ErrLogger = logger.get_logger("error")


def str2bool(v):
    # test comment
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


def checkconnection():
    """checks connection every 5 secs to see if
    it can ping the address, if it cannot it kills itself
    service will then reboot cli.py"""
    while True:
        time.sleep(5)
        returned = os.system("fping -b 12 -r 0 -t 100 " + "192.168.1.99")
        if returned == 256:
            os.kill(os.getpid(), signal.SIGKILL)


def start(HOST):
    try:
        harvesteye.ftp_server.main(HOST)
    except Exception as error:
        ErrLogger.error("Errored when launching ftp server: ", error)
        os.kill(os.getpid(), signal.SIGKILL)
        raise error

    try:
        harvesteye.reboot_server.main(HOST)
    except Exception as error:
        ErrLogger.error("Errored when launching reboot server: ", error)
        os.kill(os.getpid(), signal.SIGKILL)
        raise error

    try:
        harvesteye.harvesteye_server.main(HOST)
    except Exception as error:
        ErrLogger.error("Errored when launching harvesteye server: ", error)
        os.kill(os.getpid(), signal.SIGKILL)
        raise error


if __name__ == "__main__":
    print(harvesteye.__version__)
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", default="192.168.1.94")
    parser.add_argument("--level", default="20")
    parser.add_argument(
        "--loggers",
        type=str,
        default="Server;Segmentation;GPS;Tracker;Camera",
        help="loggers: 'Server','Segmentation','GPS','Tracker', 'Camera'",
    )
    args = parser.parse_args()

    loggers = args.loggers.split(";")
    for log in loggers:
        logger.get_logger(log).setLevel(int(args.level))

    QUIT_THREAD = Thread(target=checkconnection)
    QUIT_THREAD.daemon = False
    QUIT_THREAD.start()
    start(HOST=args.ip)
