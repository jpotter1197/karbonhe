"""
Abstract Classes used in the namespace
"""
# pylint: disable=import-error
# pylint: disable=no-name-in-module
__author__ = "Craig Cooper"
__copyright__ = "Copyright 2020, B-hive Innovations"

from abc import ABC, abstractmethod
from queue import Queue
from functools import partial
import numpy as np
import cv2
from harvesteye import logger

NSLogger = logger.get_logger("Tracker")
ErrLogger = logger.get_logger("error")


class Restriction(ABC):
    """
    used to setup different restrictions
    for calculating the movenent on the frames
    """

    # pylint: disable=too-few-public-methods
    def __call__(self, flow):
        """used to call calc_flow"""
        return self.calc_flow(flow)

    @abstractmethod
    def calc_flow(self, flow):
        """main method, used to do the calculation"""
        pass


class Tracker(ABC):
    """Tracker abstract"""

    # pylint: disable=attribute-defined-outside-init
    # pylint: disable=access-member-before-definition
    # pylint: disable=not-callable
    Restriction = None

    _defaults = {
        "images": Queue(maxsize=256),
        "fixed_inc": None,
        "current_position": None,
        "_next": None,
        "_prev": None,
        "win_height": None,
        "_restriction": None,
        "reset_pos": None,
    }

    def __init__(self, **kwargs):
        """constructor"""
        self.__dict__.update(self._defaults)
        self.__dict__.update(**kwargs)

    def __call__(self, timeout=None):
        """object call"""
        return self.images.get(timeout=timeout)

    def update(self, frames):
        """send a new frames to the farneback to get a new flow"""
        if self._next is None:
            NSLogger.debug("Setting first _next")
            self._next = frames.track()
        if self.win_height is None and self.current_position is None:
            NSLogger.debug("setting win_height and current_position")
            self.win_height = frames.track().shape[0]
            self.current_position = self.win_height / 2
        self._prev = self._next
        self._next = frames.track()
        flow = self.farneback(self._prev, self._next)
        self.calc_pos(flow)
        self.condition(frames)
        disp = frames.colour_frame
        x_rsz = int(disp.shape[0]) - int(np.round(self.current_position * (1 / 0.25)))
        cv2.line(disp, (0, x_rsz), (848, x_rsz), (255, 0, 0), 5)
        if self.reset_pos:
            cv2.line(
                disp,
                (0, int(self.win_height - self.reset_pos) * 4),
                (848, int(self.win_height - self.reset_pos) * 4),
                (0, 0, 255),
                5,
            )
        NSLogger.debug("tracker Updated")
        return disp

    def calc_pos(self, flow):
        """calculates new position from flow matrix"""
        if self.fixed_inc:
            y_comp = self.fixed_inc
        else:
            _, y_comp = self.__class__.RESTRICTION(flow)
        self.current_position += y_comp

    @property
    def farneback(self):
        """
        property that is used to send some default arguements to farneback
        """
        return partial(
            cv2.calcOpticalFlowFarneback,
            flow=None,
            pyr_scale=0.5,
            levels=1,
            winsize=15,
            iterations=1,
            poly_n=7,
            poly_sigma=1.5,
            flags=0,
        )

    @abstractmethod
    def condition(self, frames):
        """
        condition method that is used for
        determining when a frame should be captured
        """
        pass

    @classmethod
    def is_tracker(cls, mtype):
        """used to determine correct tracker in factory"""
        return cls.__name__ in mtype
