# import tools
from functools import partial
import socketserver
import socket
from threading import Thread
import cv2
import sys

# server imports
from harvesteye.server import handles
from harvesteye.server import RequestHandler
import keras
import os

# device imports
from harvesteye.camera import cameras, NoCameraFoundError
from harvesteye.gps import gps

# import segmenter
from harvesteye.segmentation import segmenter, DetectionsML, detection_factory

# import tracker
from harvesteye.tracker import AutoTracker

# import globals
import harvesteye.globals as GLOBALS
from harvesteye.utils import softwares
from harvesteye import logger

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"


class HarvesteyeHandler(RequestHandler):
    pass


NSLogger = logger.get_logger("Server")
ErrLogger = logger.get_logger("error")
EventLogger = logger.get_logger("EVENT")
sys.modules["softwares"] = softwares


# def setup_handles(CAMS, GPS, SEGMENTER, TRACKER, SOFTWARES, displays):
def setup_handles(OBJS, GPS, SOFTWARES):
    """Setting up handles"""
    HarvesteyeHandler.attact_handle("test", partial(handles.test, string="TESTHANDLE"))

    HarvesteyeHandler.attact_handle(
        "get_calibration", partial(handles.get_calibration, globals=GLOBALS)
    )

    HarvesteyeHandler.attact_handle(
        "save_roi",
        partial(
            handles.set_calibration,
            objects=OBJS,
            globals=GLOBALS,
        ),
    )

    HarvesteyeHandler.attact_handle(
        "send_metadata", partial(handles.set_metadata, globals=GLOBALS["system"])
    )

    threads = []
    funcs = []
    for serial in OBJS:
        funcs.append(partial(handles.tracking, OBJS[serial], GLOBALS))
        funcs.append(partial(handles.captured, OBJS[serial], GPS, GLOBALS))

    HarvesteyeHandler.attact_handle(
        "Request",
        partial(
            handles.start_threads,
            funcs=funcs,
            threads=threads,
        ),
    )

    HarvesteyeHandler.attact_handle("stop", partial(handles.stop, threads=threads))

    HarvesteyeHandler.attact_handle(
        "get_labelled_frame", partial(handles.get_inference, objs=OBJS)
    )

    HarvesteyeHandler.attact_handle(
        "get_colour",
        partial(
            handles.get_colour, cameras=[OBJS[serial]["camera"] for serial in OBJS]
        ),
    )
    # TODO FIX LOG FILE DELETING
    HarvesteyeHandler.attact_handle(
        "delete_logs",
        partial(
            handles.clear_file,
            files=["/mnt/data/appdata/log/harvesteye.log"],
            globals=GLOBALS,
        ),
    )

    HarvesteyeHandler.attact_handle(
        "get_version", partial(handles.get_version, SOFTWARES=SOFTWARES)
    )

    HarvesteyeHandler.attact_handle(
        "send_patch", partial(handles.send_patch, SOFTWARES=SOFTWARES)
    )

    HarvesteyeHandler.attact_handle(
        "get_potatoes",
        partial(
            handles.get_detection_data,
            objs=OBJS,
        ),
    )

    HarvesteyeHandler.attact_handle(
        "get_serials", partial(handles.get_serials, serials=[serial for serial in OBJS])
    )


"""Starting Server"""


def main(HOST=socket.gethostbyname(socket.gethostname()), PORT=9000):
    SOFTWARES = softwares.Softwares(base_path="/mnt/data/")

    """
    Init Server objects
    """
    try:
        """
        doc string to fix patching mess
        """
        CAMS = cameras()
        GPS = gps()

        DetectionsML.HEIGHT_MODEL = keras.models.load_model(
            "/mnt/data/appdata/depth_model.h5"
        )
        DetectionsML.SIZEBAND_MODEL = keras.models.load_model(
            "/mnt/data/appdata/sizeband_model.h5"
        )
        DetectionsML.WEIGHT_MODEL = keras.models.load_model(
            "/mnt/data/appdata/weight_model.h5"
        )
        DetectionsML.CALCULATION_CLASS = detection_factory(GLOBALS["system"].detection)
        OBJS = {}
        for CAM in CAMS:
            TRACKER = AutoTracker(fixed_inc=GLOBALS[CAM.serial].fixed_inc)
            SEGMENTER = segmenter(
                "/mnt/data/appdata/yolo",
                inference_res=GLOBALS[CAM.serial].inference_res,
                confidence_thresh=GLOBALS[CAM.serial].confidence_thresh,
                threshold_thresh=GLOBALS[CAM.serial].threshold_thresh,
            )
            SEGMENTER.detections = DetectionsML
            OBJS[CAM.serial] = {
                "camera": CAM,
                "tracker": TRACKER,
                "segmenter": SEGMENTER,
                "displays": {"tracker": None, "inference": None, "captured": None},
            }
        NSLogger.info("Setting up server handles with objects")
        setup_handles(OBJS, GPS, SOFTWARES)
        socketserver.TCPServer.allow_reuse_address = True
        server = socketserver.TCPServer((HOST, PORT), HarvesteyeHandler)
        # server.serve_forever()
        server_thread = Thread(target=server.serve_forever)
        server_thread.daemon = False
        EventLogger.info("[EVENT:ServerStatus],{'STATUS':1}")
        server_thread.start()
        if NSLogger.level == 10:
            while True:
                for serial in OBJS:
                    for display in OBJS[serial]["displays"]:
                        if OBJS[serial]["displays"][display] is not None:
                            cv2.imshow(
                                serial + " " + display,
                                OBJS[serial]["displays"][display],
                            )
                            cv2.waitKey(1)
    except NoCameraFoundError as error:
        EventLogger.info("[EVENT:NoCameraFound],{'STATUS':0}")
        NSLogger.error(error, exc_info=True)
        ErrLogger.error(error, exc_info=False)
        raise error


if __name__ == "__main__":
    logger.get_logger("EVENT").setLevel(30)
    logger.get_logger("Server").setLevel(10)
    main()  # HOST="192.168.1.94")
