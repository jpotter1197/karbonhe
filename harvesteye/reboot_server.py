# import tools
import subprocess
import socketserver
import socket
from threading import Thread
import re
import os

# server imports
from shutil import copyfile
from harvesteye.server import RequestHandler


class RebootHandler(RequestHandler):
    pass


def reboot(self, data):
    """reboots system"""
    subprocess.call("/sbin/reboot", shell=True)


def restore(self, data):
    """copies restore patch to patch location"""
    base = "/mnt/data/SoftwareUpdater/"
    files = os.listdir(base)
    for file in files:
        if re.findall(r"[a-zA-Z]+@*[0-9]-*[0-9].*[0-9].tgz", file):
            copyfile(os.path.join(base, file), os.path.join(base, "patches", file))


RebootHandler.attact_handle("reboot", reboot)

RebootHandler.attact_handle("restore", restore)


def main(HOST=socket.gethostbyname(socket.gethostname()), PORT=9999):
    print("running server")
    socketserver.TCPServer.allow_reuse_address = True
    server = socketserver.TCPServer((HOST, PORT), RebootHandler)
    # server.serve_forever()
    server_thread = Thread(target=server.serve_forever)
    server_thread.daemon = False
    server_thread.start()


if __name__ == "__main__":
    main()
