import argparse
import os
import json
import shutil
import tarfile

from dirtools import Dir, DirState

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--version")
    parser.add_argument("--name")
    parser.add_argument("--factory_version")
    parser.add_argument("--install_path")
    args = parser.parse_args()

    os.system(
        "aws s3 sync --delete s3://bhivedistributions/pyhe/builds/{} dist/tmp".format(
            args.factory_version
        )
    )
    old = DirState(Dir(os.path.join(os.getcwd(), "dist", "tmp")))
    os.system(
        "aws s3 sync --delete s3://bhivedistributions/pyhe/builds/{} dist/tmp".format(
            args.version
        )
    )
    os.system(
        "aws s3 cp s3://bhivedistributions/pyhe/builds/{}/version.txt dist/tmp".format(
            args.version
        )
    )
    new = DirState(Dir(os.path.join(os.getcwd(), "dist", "tmp")))
    changes = new - old

    # create patch folder/json name
    patch_format = "{0}@{1}-{2}".format(args.name, args.factory_version, args.version)

    # create folder
    os.mkdir(os.path.join(os.getcwd(), "dist", patch_format))
    changes["updated"].append("version.txt")
    # move all changed files to patch folder
    for key in ["created", "updated"]:
        for idx, file in enumerate(changes[key]):
            shutil.copy(
                os.path.join(os.getcwd(), "dist", "tmp", file),
                os.path.join(os.getcwd(), "dist", patch_format, os.path.basename(file)),
            )

    # add install dir to each file location in json
    for key in changes.keys():
        for idx, file in enumerate(changes[key]):
            if file:
                changes[key][idx] = os.path.join(args.install_path, file)

    # create json
    with open(
        os.path.join(os.getcwd(), "dist", patch_format, patch_format + ".json"), "wb"
    ) as f:
        f.write(json.dumps(changes).encode())

    # create cmd file
    with open(os.path.join(os.getcwd(), "dist", patch_format, "cmd.txt"), "w") as f:
        f.write(
            "1 chown root:root {}\n".format(os.path.join(args.install_path, args.name))
        )
        f.write("1 chmod 0500 {}\n".format(os.path.join(args.install_path, args.name)))
        f.write("1 rm -rf {}\n".format(os.path.join(args.install_path, ".Trash-*")))

    # tar the patch
    with tarfile.open(
        os.path.join(os.getcwd(), "dist", patch_format + ".tgz"), "w:gz"
    ) as tar:
        tar.add(os.path.join(os.getcwd(), "dist", patch_format), arcname="")

    # clean up
    shutil.rmtree(os.path.join(os.getcwd(), "dist", patch_format))
    shutil.rmtree(os.path.join(os.getcwd(), "dist", "tmp"))
