import numpy as np
import cv2
import threading
import argparse
import time
import sys
from queue import Empty
from harvesteye.tracker import AutoTracker
from harvesteye.tracker.abstracts import Tracker
from harvesteye.tracker.abstracts import Restriction
from harvesteye.camera.abstracts import Frame
from harvesteye.camera._frames import Mp4Frame


def track(cam, tracker):
    def tracking(tracker, cam, displays):
        """This is constantly feen frames from the camera to the Tracker"""
        while True:
            try:
                time.sleep(0.05)
                ret, frame = cam.read()
                frame = Mp4Frame(frame=frame)
                #frame.set_mask([155, 0, 848, 480])
                displays["tracker"] = tracker.update(frame)

            except RuntimeError as error:
                if "Frame didn't arrived within 1000" in str(error):
                    print("End of bag")
                    break

    def captured(tracker, displays):
        """This will grab any 'captured frames' from the tracker"""
        while True:
            try:
                tracker(5)
            except Empty:
                pass
    DISPLAYS = {
        "tracker": None,
        # "captured": None,
    }

    TRACKERTHREAD = threading.Thread(target=tracking, args=(TRACKER, CAM, DISPLAYS))
    TRACKERTHREAD.start()
    CAPTURETHREAD = threading.Thread(target=captured, args=(TRACKER, DISPLAYS))
    CAPTURETHREAD.start()

    with open("slippage.txt", "w") as output:
        output.write("")
    while True:
        for DISPLAY in DISPLAYS:
            if DISPLAYS[DISPLAY] is not None:
                cv2.imshow(DISPLAY, DISPLAYS[DISPLAY])
        if cv2.waitKey(1) & 0xFF == ord(" "):
            correct_position = 480 - 480 / 3
            current_position = TRACKER.current_position * 4
            slippage = np.round(1 - current_position / correct_position, 4)
            cv2.putText(
                DISPLAYS[DISPLAY],
                str(slippage),
                (10, 40),
                cv2.FONT_HERSHEY_SIMPLEX,
                1,
                (255, 255, 255),
                2,
            )
            cv2.imshow("Capture", DISPLAYS[DISPLAY])
            with open("slippage.txt", "a") as output:
                output.write(f"{str(slippage)}\n")


if __name__ == "__main__":
    try:
        CAM = cv2.VideoCapture("/home/michael/Videos/harvest mp4s/Kwatro/video.mp4")
        TRACKER = AutoTracker()
        track(CAM, TRACKER)
    except Exception:
        print("no video found")
