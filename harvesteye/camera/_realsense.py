"""
Realsense class for any camera class.py:
camera functionality
"""

__author__ = "Craig Cooper"
__copyright__ = "Copyright 2020, B-hive Innovations"

import numpy as np
import pyrealsense2 as rs
import cv2
import os
from harvesteye.camera.abstracts import Camera, Frame, DistanceError, Borg
import warnings
from harvesteye import logger

EventLogger = logger.get_logger("EVENT")

NSLogger = logger.get_logger("Camera")
ErrLogger = logger.get_logger("error")


class EndOfBagError(Exception):
    pass


class FrameMeta(type(Frame), type(rs.composite_frame)):
    pass


class CompositeFrame(Frame, rs.composite_frame, metaclass=FrameMeta):
    """
    this class add frame abstract class to rs.composite_frame.
    meaning we can build a realsense frames
    object with our methods to keep things working
    """

    @property
    def colour_frame(self):
        """
        This method is basicly a facade for creating a colour image
        args: None
        return: numpy ndarray of type np.uint8 with the shape of the image.
        """
        colour_frame = rs.composite_frame.get_color_frame(self)
        colour_frame = np.asanyarray(colour_frame.get_data(), np.uint8)
        return cv2.cvtColor(colour_frame, cv2.COLOR_RGB2BGR)

    @property
    def depth_frame(self):
        """
        This method is basicly a facade for creating a colour image
        args: None
        return: numpy ndarray of type np.uint8 with the shape of the image.
        """
        colorizer = rs.colorizer()
        colorized_depth = np.asanyarray(
            colorizer.colorize(self.get_depth_frame()).get_data()
        )
        return colorized_depth

    def get_distance(self, point1, point2, angle=None, center_point=None):
        """
        This method is will get the distance in
         mm from two points in the image.
        args: point1, point2
        return: float, this is in units of mm
        """
        try:
            if angle is None:
                p1 = self._point3d(point1, center_point)
                p2 = self._point3d(point2, center_point)
                if p1 and p2:
                    dist = (
                        np.sqrt(
                            np.power((p1[0] - p2[0]), 2)
                            + np.power((p1[1] - p2[1]), 2)
                            + np.power((p1[2] - p2[2]), 2)
                        )
                        * 1000
                    )
                    return dist
                return None

            # p1 must be the top
            # p2 must be the bottom
            # Need an angle if statement here for which rounding to use.
            if angle <= 90:
                # This is for a tuber angled to the right.
                p1_short = (np.floor(point1[0]), np.ceil(point1[1]))
                p2_short = (np.ceil(point2[0]), np.floor(point2[1]))

                p1_long = (np.ceil(point1[0]), np.floor(point1[1]))
                p2_long = (np.floor(point2[0]), np.ceil(point2[1]))

            if angle > 90:
                # This is for a tuber angled to the left.
                p1_short = (np.ceil(point1[0]), np.ceil(point1[1]))
                p2_short = (np.floor(point2[0]), np.floor(point2[1]))

                p1_long = (np.floor(point1[0]), np.floor(point1[1]))
                p2_long = (np.ceil(point2[0]), np.ceil(point2[1]))

            # Get 3d maps of long and short points.
            p1_short = self._point3d(p1_short, center_point)
            p2_short = self._point3d(p2_short, center_point)
            p1_long = self._point3d(p1_long, center_point)
            p2_long = self._point3d(p2_long, center_point)

            if not any([p1_short, p1_long, p2_short, p2_long]):
                NSLogger.debug(
                    "Distance couldn't be calculated " "due to point returning none",
                    point1,
                    point2,
                )
                return None

            # Calculate shortest possible distance.
            dist_short = (
                np.sqrt(
                    np.power((p1_short[0] - p2_short[0]), 2)
                    + np.power((p1_short[1] - p2_short[1]), 2)
                    + np.power((p1_short[2] - p2_short[2]), 2)
                )
                * 1000
            )

            # Calculate longest possible distance.
            dist_long = (
                np.sqrt(
                    np.power((p1_long[0] - p2_long[0]), 2)
                    + np.power((p1_long[1] - p2_long[1]), 2)
                    + np.power((p1_long[2] - p2_long[2]), 2)
                )
                * 1000
            )

            # Get the fraction of the difference to use.
            dist_diff = dist_long - dist_short
            dist_diff_x = (
                dist_diff
                * np.cos(np.deg2rad(angle))
                * ((point1[0] % 1) + (point2[0] % 1))
                / 2
            )
            dist_diff_y = (
                dist_diff
                * np.sin(np.deg2rad(angle))
                * ((point1[1] % 1) + (point2[1] % 1))
                / 2
            )
            dist = dist_short + np.sqrt(dist_diff_x ** 2 + dist_diff_y ** 2)

            return dist

        except DistanceError:
            NSLogger.debug("Cannot get depth value, returning None")
            return None
        except Exception as error:
            NSLogger.exception(
                "Errored while calculating distance between %s & %s", point1, point2
            )
            ErrLogger.error("Errored while calculating distance")
            raise error

    def _point3d(self, point, center_point=None, mask=(0, 0)):
        """
        This method will attempt to get a 3d point from base on pixel coord.
        if the depth at the pixel location is 0 (no depth info)
        then recurse down with \
        a bigger mask untill you find a depth to use MAX
        mask size is 11 by 11, any more then this \
        could give you bad sizing
        args: point
        return: tuple, (x,y,z)
        """
        try:
            point = (int(point[0]), int(point[1]))
            if mask > (9, 9):
                raise DistanceError
            depths = np.zeros((mask[0] * 2 + 1, mask[1] * 2 + 1))
            for x in range(-mask[0], mask[0] + 1):
                for y in range(-mask[1], mask[1] + 1):
                    depths[x + mask[0], y + mask[1]] = self.depth(
                        tuple(map(sum, zip(point, (x, y)))), center_point
                    )[2]
            depths[depths == 0] = np.nan
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=RuntimeWarning)
                mean = np.nanmean(depths)
            if not np.isnan(mean):
                ret = self.depth((point[0], point[1]), center_point, mean)
                return ret
            return self._point3d(point, center_point=center_point, mask=tuple(map(sum, zip(mask, (1, 1)))))
        except TypeError:
            return None

    def depth(self, point, center_point=None, mean=None):
        """
        This method is used to get a single 3d point
        in space base on pixel coord
        """
        try:
            depth_frame = self.get_depth_frame()
            if center_point is None:
                center_point = point
            if mean is None:
                point_z = depth_frame.get_distance(center_point[0], center_point[1]) - 0.025
                if point_z <= 0:
                    point_z = 0
            if mean is not None:
                point_z = mean
            depth_intrin = depth_frame.profile.as_video_stream_profile().intrinsics
            result = rs.rs2_deproject_pixel_to_point(depth_intrin, [point[0], point[1]], point_z)
            return result

        except RuntimeError:
            if "out of range" in str(RuntimeError):
                NSLogger.warning("out of range error been caught, handled")
                return 0
        except Exception as error:
            NSLogger.exception("Errored while calculating depth for point %s", point)
            ErrLogger.error("Errored while calculating depth")
            raise error

    def save(self, save_path=os.getcwd(), filename=""):
        saver = rs.save_single_frameset(
            os.path.join(save_path, "sample_" + str(filename) + "_")
        )
        saver.process(self)


class Realsense(Camera, Borg):
    """
    Realsense Camera
    """

    _defaults = {
        "xres": 848,
        "yres": 480,
        "fps": 30,
        "_pipeline": None,
        "_profile": None,
        "config": None,
        "bagfile": None,
        "_intrinsics": None,
        "max_distance": 2,
        "play_back": None,
        "realtime": True,
        "repeat": False,
        "serial": None,
    }

    def __init__(self, **kwargs):
        """
        This Method is used to init the Borg,
        set default values and then start the camera pipeline
        args: kwargs
        return: None
        """
        # if "bagfile" not in kwargs.keys():
        #    Borg.__init__(self)
        if self.__dict__ == {}:
            NSLogger.debug("Settings up default variables. %s", self._defaults)
            self.__dict__.update(self._defaults)
        else:
            NSLogger.debug("Camera already has been loaded once")
        self.start(kwargs)

    def start(self, kwargs):
        """
        This Method is used to update kwargs and start the camera pipeline.
        NOTE: As we are using Borg design pattern we do not want to be
        the __dict__ \
        on init of each new object created only when certain condition are met.

        cam = Camera() # creates new camera with shared vars
        of another is exists
        cam = Camera(kwargs) # changes settings for all cameras

        All Cameras should pull of the same pipeline
        args: kwargs
        return: None
        """
        try:
            if self._pipeline is not None:
                self.stop()
            if kwargs is not {}:
                self.__dict__.update(kwargs)
                self._pipeline = rs.pipeline()
                self._create_config()
                self._profile = self._pipeline.start(self.config)
                if self.bagfile:
                    device = self._pipeline.get_active_profile().get_device()
                    self.play_back = rs.playback.as_playback(device)
                    self.play_back.set_real_time(self.realtime)
                    if not self.realtime:
                        NSLogger.info(
                            "Reading streams from bagfile %s in playback", self.bagfile
                        )
                    else:
                        NSLogger.info(
                            "Reading streams from bagfile %s in realtime", self.bagfile
                        )
                    if self.repeat:
                        NSLogger.info("Bagfile %s is set on repeat", self.bagfile)
                else:
                    NSLogger.info("Reading streams from device")
        except Exception as error:
            NSLogger.exception("Errored while starting camera %s", self.__dict__)
            ErrLogger.error("Errored while starting camera")
            EventLogger.info("[EVENT:NoCameraFound],{'STATUS':0}")
            ctx = rs.context()
            devices = ctx.query_devices()
            for dev in devices:
                dev.hardware_reset()
            raise error

    def stop(self):
        """
        This method is used to shutdown the pipeline/camera
        args: None
        return: None
        """
        self._pipeline.stop()

    def _create_config(self):
        """
        creates rs.config used for setting up the
        camera pipeline with certain settings
        """
        try:
            self.config = rs.config()
            if self.serial is not None:
                self.config.enable_device(self.serial)
            self._intrinsics = None
            if self.bagfile:
                ctx = rs.context()
                play_back = ctx.load_device(self.bagfile)
                sensors = play_back.query_sensors()
                profile = sensors[0].get_stream_profiles()[0]
                self.xres = profile.as_video_stream_profile().width()
                self.yres = profile.as_video_stream_profile().height()
                devices = ctx.query_devices()
                self.serial = devices.back().get_info(rs.camera_info.serial_number)
                ctx.unload_device(self.bagfile)
                rs.config.enable_device_from_file(
                    self.config, self.bagfile, repeat_playback=self.repeat
                )

            streams = [
                [rs.stream.depth, rs.format.z16],
                [rs.stream.color, rs.format.rgb8],
            ]
            for stream in streams:
                self.config.enable_stream(
                    stream[0], self.xres, self.yres, stream[1], self.fps
                )
        except Exception as error:
            NSLogger.exception("Errored while creating camera config", self.__dict__)
            ErrLogger.error("Errored while creating camera config")
            raise error

    @property
    def intrinsics(self):
        """
        grabs intrinsics from the camera, used for working out distances
        """
        try:
            if self._intrinsics:
                return self._intrinsics
            cfg = self._pipeline.get_active_profile()
            profile = cfg.get_stream(rs.stream.depth)
            self._intrinsics = profile.as_video_stream_profile().get_intrinsics()
            return self._intrinsics
        except Exception as error:
            raise error

    def __call__(self):
        """
        Retrieves the next frame from the camera, If specified in the
        constructor it:
        * aligns
        * filters
        :return: Returns rgb and depth as numpy arrays (uint8 and double),
        and the frame number
        """
        try:
            if not self.realtime:
                self.play_back.resume()
            frames = self._pipeline.wait_for_frames(1000)
            if not self.realtime:
                self.play_back.pause()
            frames = rs.align(rs.stream.color).process(frames)
            # frames = _apply_rs_filters(frames, self.max_distance)
            frames = CompositeFrame(frames)
            frames.max_distance = self.max_distance
            NSLogger.debug("returing frame %s", frames.frame_number)
            return frames
        except RuntimeError as error:
            EventLogger.info("[EVENT:NoCameraFound],{'STATUS':0}")
            if (
                "Frame didn't arrive within 1000" in str(error)
                and self.bagfile is not None
                and not self.repeat
            ):
                NSLogger.info("No more frames in bagfile")
            else:
                NSLogger.exception(
                    "Errored while grabbing frame from pipeline", self.__dict__
                )
                ErrLogger.error("Errored while grabbing frame from pipeline")
            ctx = rs.context()
            devices = ctx.query_devices()
            for dev in devices:
                dev.hardware_reset()
            raise error
        except Exception as error:
            NSLogger.exception(
                "Errored while grabbing frame from pipeline", self.__dict__
            )
            ErrLogger.error("Errored while grabbing frame from pipeline")
            EventLogger.info("[EVENT:NoCameraFound],{'STATUS':0}")
            ctx = rs.context()
            devices = ctx.query_devices()
            for dev in devices:
                dev.hardware_reset()
            raise error

    @classmethod
    def uses_file(cls, kwargs):
        return "bagfile" in kwargs.keys()
