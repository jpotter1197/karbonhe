"""Helper Script for tracking"""
# pylint: disable=import-error
# pylint: disable=no-name-in-module
# pylint: disable=superfluous-parens
import threading
from queue import Empty

import cv2
from harvesteye.camera import Realsense
from harvesteye.tracker import AutoTracker


def track(cam, tracker):
    def tracking(tracker, cam, displays):
        """This is constantly feen frames from the camera to the Tracker"""
        while True:
            try:
                frames = cam()
                # frames.set_mask([150,0,1000,720])
                # frames.set_mask([100,100,200,200])
                displays["tracker"] = tracker.update(frames)

            except RuntimeError as error:
                if "Frame didn't arrived within 1000" in str(error):
                    print("End of bag")
                    break

    def captured(tracker, displays):
        """This will grab any 'captured frames' from the tracker"""
        while True:
            try:
                frames = tracker(5)
                displays["captured"] = frames.masked
            except Empty:
                pass

    DISPLAYS = {
        "tracker": None,
        "captured": None,
    }
    TRACKERTHREAD = threading.Thread(target=tracking, args=(TRACKER, CAM, DISPLAYS))
    TRACKERTHREAD.start()
    CAPTURETHREAD = threading.Thread(target=captured, args=(TRACKER, DISPLAYS))
    CAPTURETHREAD.start()
    while True:
        for DISPLAY in DISPLAYS:
            if DISPLAYS[DISPLAY] is not None:
                cv2.imshow(DISPLAY, DISPLAYS[DISPLAY])
        cv2.waitKey(1)


if __name__ == "__main__":
    CAM = Realsense(bagfile="/home/b-hive/Videos/video1.bag")
    print("cam:", CAM)
    TRACKER = AutoTracker()
    track(CAM, TRACKER)
