"""
Restrictions module holding child restriction
classes used for different flow calculations
"""
# pylint: disable=import-error
# pylint: disable=no-name-in-module
import numpy as np
import cv2
import scipy.stats as stats
from harvesteye.tracker.abstracts import Restriction

np.warnings.filterwarnings("ignore")


class CalcNegativeRestrictedFlow(Restriction):
    """restriction images pixel velocitys that are negitive"""

    # pylint: disable=too-few-public-methods
    def calc_flow(self, flow):
        """
        Uses Farneback's dense optical flow to estimate the x and y
        components of each pixel's velocity
        We return the mean of these to approximate how many pixels it has
        moved left/right and up/down
        :return: the mean x and y components
        y_comp is restricted to only increase not decrease
        """
        # flow[:, :, 1] is y flow[:, :, 0] is x
        y_flow = flow[:, :, 1]
        x_flow = flow[:, :, 0]
        nearzero = 0.01
        y_flow[np.logical_and(y_flow > -nearzero, y_flow < nearzero)] = np.nan

        z_score = stats.zscore(y_flow, nan_policy="omit")
        y_flow[z_score < -2] = np.nan
        y_flow[z_score > 0.1] = np.nan

        # Arms will have a large x component too, so ignore these
        """
        Reverse these as they give the flow from
         next to previous rather than previous to next
        """
        x_comp = -np.mean(x_flow)
        y_comp = -np.nanmean(y_flow)
        # If 99% of the image is nan, then make it 0 to remove little glitches
        if (
            cv2.countNonZero(np.isnan(y_flow).astype(np.uint8))
            / (flow.shape[0] * flow.shape[1])
            > 0.99
        ):
            y_comp = 0
        if np.isnan(y_comp):
            y_comp = 0
        return x_comp, y_comp


class CalcPositiveRestrictedFlow(Restriction):
    """restriction images pixel velocitys that are postitive"""

    # pylint: disable=too-few-public-methods
    def calc_flow(self, flow):
        """
        Uses Farneback's dense optical flow to estimate the x and y
        components of each pixel's velocity
        We return the mean of these to approximate how many pixels it has
        moved left/right and up/down
        :return: the mean x and y components
        y_comp is restricted to only increase not decrease
        """
        try:
            # flow[:, :, 1] is y flow[:, :, 0] is x
            y_flow = flow[:, :, 1]
            x_flow = flow[:, :, 0]
            nearzero = 0.01
            y_flow[np.logical_and(y_flow > -nearzero, y_flow < nearzero)] = np.nan

            z_score = stats.zscore(y_flow, nan_policy="omit")
            y_flow[z_score < -0.1] = np.nan
            y_flow[z_score > 2] = np.nan

            # Arms will have a large x component too, so ignore these
            """
            Reverse these as they give the flow from next
            to previous rather than previous to next
            """
            x_comp = -np.mean(x_flow)
            y_comp = -np.nanmean(y_flow)
            """
            If 99% of the image is nan, then
            make it 0 to remove little glitches
            """
            if (
                cv2.countNonZero(np.isnan(y_flow).astype(np.uint8))
                / (flow.shape[0] * flow.shape[1])
                > 0.99
            ):
                y_comp = 0
            if np.isnan(y_comp):
                y_comp = 0
            return x_comp, y_comp
        except Exception as error:
            raise error


class CalcFreeFlow(Restriction):
    """restriction that doesnt restriction any pixel velocity"""

    # pylint: disable=too-few-public-methods
    def calc_flow(self, flow):
        """
        Uses Farneback's dense optical flow to estimate the x and y
        components of each pixel's velocity
        We return the mean of these to approximate how many pixels it has
        moved left/right and up/down
        :return: the mean x and y components
        y_comp is free to move in any y directions
        """
        try:
            x_comp = -np.mean(flow[:, :, 0])
            y_comp = -np.nanmean(flow[:, :, 1])
            return x_comp, y_comp
        except Exception as error:
            raise error
