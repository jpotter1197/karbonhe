from harvesteye.server import ClientSocket
import socket
from xml.etree.ElementTree import Element, tostring


def dict_to_xml(tag, d):

    elem = Element(tag)
    for key, val in d.items():
        elem.attrib[key] = str(val)
    return elem


# metadata = {"Grower": "B-Hive", "Field": "Test", "Variety": "test"}
# metadata = dict_to_xml("send_metadata", metadata)
# metadata = tostring(metadata)
# version = {"crop_type": "potato"}
# version = dict_to_xml("get_version", version)
# version = tostring(version)


HOST, PORT = socket.gethostbyname(socket.gethostname()), 9000
# HOST, PORT = "192.168.1.94", 9000
client = ClientSocket()
client.connect((HOST, PORT))
calib = {
    "Height": 10,
    "Confidence": 50,
    "ROI1": "0,0,0,0",
    "ROI2": "0,0,0,0",
    "FixedInc": 2,
    "Inference": 416,
    "TimeBetween": 200,
    "BBox": "BboxDetection",
}
calib = dict_to_xml("save_roi", calib)
calib = tostring(calib)

client.send_message(calib)
print(client.read_message())
client.send_message(b"get_serials")
serials = client.read_message().decode().split("/")
for serial in serials:
    client.send_message(tostring(dict_to_xml("get_calibration", {"serial": serial})))
    print(client.read_message())

client.send_message(b"stop")

# while True:
#     client = ClientSocket()
#     client.connect((HOST, PORT))
#     client.send_message(b"get_potatoes")
#     print(client.read_message())
