"""Module to work directly with image and depth frames."""
import numpy as np
import cv2
from harvesteye.camera.abstracts import Frame


class Mp4Frame(Frame):
    """MP4 Frame for testing optical flow."""

    def __init__(self, frame):
        self.frame = frame

    @property
    def colour_frame(self):
        return self.frame

    @property
    def depth_frame(self):
        return None

    def get_distance(self, point1, point2, center_point):
        return None

    def depth(self, point):
        return None


class FrameFromFile(Frame):
    """Loads in an image and an array of depth information."""

    def __init__(self, image_path, depth_path=None):
        self.image_path = image_path
        self.depth_path = depth_path

    @property
    def colour_frame(self):
        return cv2.imread(self.image_path)

    @property
    def depth_frame(self):
        image = cv2.imread(self.image_path)
        depth = np.zeros(image.shape[:2], dtype="uint8") * 255
        return depth

    def get_distance(self, point1, point2, center_point):
        """This method is will get the distance in mm from two points in the image.
        args: point1, point2
        return: float, this is in units of mm
        """
        if self.depth_path is None:
            return 0
        point_map = np.load(self.depth_path)
        image = cv2.imread(self.image_path)
        point_map = np.reshape(point_map, image.shape)

        point1 = (int(point1[1]), int(point1[0]))
        point2 = (int(point2[1]), int(point2[0]))

        try:
            values1 = point_map[point1]
            values2 = point_map[point2]

            # Avoids holes in depth map.
            count = 1
            while any(values1) == 0:
                if (count % 2) == 0:
                    values1 = point_map[(point1[0] + count, point1[1])]
                    count += 1
                else:
                    values1 = point_map[(point1[0], point1[1] + count)]
                    count += 1
            count = 1
            while any(values2) == 0:
                if (count % 2) == 0:
                    values2 = point_map[(point2[0] + count, point2[1])]
                    count += 1
                else:
                    values2 = point_map[(point2[0], point2[1] + count)]
                    count += 1

            return (
                np.sqrt(
                    np.power((values1[0] - values2[0]), 2)
                    + np.power((values1[1] - values2[1]), 2)
                )
                * 1000
            )
        except Exception:
            return 0

    def depth(self, point):
        """Gets the depth for the point map at a given point."""
        point_map = np.load(self.depth_path)
        image = cv2.imread(self.image_path)
        point_map = np.reshape(point_map, image.shape)
        point = (int(point[1]), int(point[0]))

        try:
            values = point_map[point]

            # Avoids holes in depth map.
            count = 1
            while any(values) == 0:
                if (count % 2) == 0:
                    values = point_map[(point[0] + count, point[1])]
                    count += 1
                else:
                    values = point_map[(point[0], point[1] + count)]
                    count += 1
            return values
        except Exception:
            return 0
