FROM ubuntu:16.04

RUN apt-get update && apt-get install -y software-properties-common && add-apt-repository ppa:deadsnakes/ppa && \
    apt-get update && apt-get install -y python3.6 python3.6-dev python3-pip && apt-get install -y libsystemd-dev && \
    apt-get install -y pkg-config && apt-get install -y git && apt-get install -y awscli 
RUN dpkg --add-architecture i386 && apt-get update && apt-get install -y libusb-1.0.0
RUN apt-get install ffmpeg libsm6 libxext6  -y

RUN ln -sfn /usr/bin/python3.6 /usr/bin/python3 && ln -sfn /usr/bin/python3 /usr/bin/python && ln -sfn /usr/bin/pip3 /usr/bin/pip

RUN pip install globster
RUN pip install --upgrade pip

