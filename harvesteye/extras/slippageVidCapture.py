import cv2
import os
import time


def list_ports():
    last_cam = 0
    for dev_port in range(11):
        camera = cv2.VideoCapture(dev_port)

        if not camera.isOpened():
            print("No camera at port %s." % dev_port)
        else:
            is_reading, img = camera.read()
            if is_reading:
                camera.release()
                print("Port %s is working and reads images." % dev_port)
                last_cam = dev_port
            else:
                print("Port %s for camera is present but does not read." % dev_port)

    return last_cam


if __name__ == "__main__":
    port = list_ports()
    print("Please enter desired port number:")
    port_selection = input()
    cap = cv2.VideoCapture(int(port_selection))
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 848)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
    out1 = cv2.VideoWriter_fourcc(*"mp4v")
    out = cv2.VideoWriter("video.mp4", out1, 30, (848, 480))
    time_end = time.time() + 30
    print("Video capture started.")
    while time.time() < time_end:
        ret, frame = cap.read()
        if not ret:
            print("Can't receive frame.")
            break
        #cv2.imshow("Frames", frame)
        out.write(frame)
        if cv.waitKey(1) == ord('q'):
            break

    print("30 second video captured.")
    cap.release()
    out.release()
    cv2.destroyAllWindows()
