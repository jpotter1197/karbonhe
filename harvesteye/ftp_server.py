"""ftp_client.py:
File transfer server
"""

__author__ = "Craig Cooper, Phil Markall"
__copyright__ = "Copyright 2020, B-hive Innovations"
import time
from threading import Thread
from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer
import socket


class FailedToAssignError(Exception):
    """CustomeError Class"""


def main(HOST=socket.gethostbyname(socket.gethostname()), PORT=2001):
    """runs server"""
    while True:
        try:
            authorizer = DummyAuthorizer()
            authorizer.add_user(
                "harvesteyeuser", "updates123", "/mnt/data", perm="elradfmwMT"
            )
            handler = FTPHandler
            handler.authorizer = authorizer
            try:
                server = FTPServer((HOST, PORT), handler)
                # server = FTPServer(("192.168.1.94", 2001), handler)
            except Exception:
                raise FailedToAssignError("Cannot assign requested address")
            server_thread = Thread(target=server.serve_forever)
            server_thread.daemon = False
            server_thread.start()
        except FailedToAssignError:
            # Wait 5 seconds and try again
            time.sleep(5)
            continue
        except Exception as error:
            raise error
        break


if __name__ == "__main__":
    main()
