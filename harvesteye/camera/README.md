
# Camera

Foobar is a Python library for dealing with word pluralization.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install harvesteye.

```bash

pip install git+https://github.com/user/repo.git@branch
```

## Usage

```python
import harvesteye
from harvesteye.camera import Realsense
from harvesteye.camera.helper import display

cam = Realsense()
while True:
try:
    frames = cam()
    print(frames.get_distance((0,100),(200,100)))
    display(frames)
except RuntimeError as error:
    if "Frame didn't arrived within 1000" in str(error):
        print("End of bag")
        break
    raise error
```

### Creating your own Camera/Frame class

```python
from havesteye.camera.abstracts import Camera, Frame

class Your_Frame(Frame):
    """
    abstract methods and properties the is coupled with the camera being used
    IE Each camera should have its related Frame to deal
    A Frame class can be created to deal with over situations, for example reading from files
    NOTE: if the camera returns there own object type to wrap the frames, this object should \
    inherit from that class too. 
    WARNING: metaclass cconflict will happen, see harvesteye.camera._realsense.FrameMeta to see
    how the the realsense frame is handled.
    """
    @property
    def colour_frame(self) -> "numpy.ndarray[np.uint8], shape (xres,yres,3)":
        """
        This method is basicly a facade for creating a colour image
        args: None
        return: numpy ndarray of type np.uint8 with the shape of the image.
        """
        pass

    def get_distance(self, point1: tuple, point2: tuple) -> float:
        """
        This method is will get the distance in mm from two points in the image.
        args: point1, point2
        return: float, this is in units of mm
        """
        pass

class your_camera(Camera):
    _defaults = {
        "xres": 848,
        "yres": 480,
        "fps": 30
    }
    def start(self, kwargs) -> None:
        """
        This Method is used to update kwargs and start the camera pipeline.
        NOTE: As we are using Borg design pattern we do not want to be changing the __dict__ \
        on init of each new object created only when certain condition are met.
        
        cam = Camera() # creates new camera with shared vars of another is exists
        cam = Camera(kwargs) # changes settings for all cameras
        
        All Cameras should pull of the same pipeline
        args: kwargs
        return: None
        """
        pass

    def stop(self) -> None:
        """
        This method is used to shutdown the pipeline/camera
        args: None
        return: None
        """
        pass

    def __call__(self) -> Frame:
        """
        This method is used to camera is called a Frame should be returned basicilly get_frame().
        args: None
        return: Frame 
        NOTE: Frame object needs to be created to handle the camera properties
        """
        pass

```

