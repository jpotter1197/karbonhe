"""unit testing for realsense class"""
import unittest
import time
import os

import cv2
import numpy as np
from harvesteye.camera import Realsense

WORKING_DIR = os.getcwd()


class TestPixelInterp(unittest.TestCase):
    def setUp(self):
        cam = Realsense(bagfile="/home/b-hive/Videos/HE004-video.bag", repeat=True)
        # FRAME = FrameFromFile("/home/b-hive/darknet/data/onion/obj/tboogsvcsewymdakvsrl.jpg")
        time.sleep(1)
        self.Frame = cam()

    def tearDown(self):
        pass

    def test_DecimalTestFlatLineHalf(self):
        center_point = (0, 0)
        angle = 90
        long = ((200, 200), (200, 210))
        short = ((200, 201), (200, 209))
        decimal = ((200, 200.5), (200, 209.5))

        dist_long = self.Frame.get_distance(*long, angle, center_point)
        dist_short = self.Frame.get_distance(*short, angle, center_point)
        dist_decimal = self.Frame.get_distance(*decimal, angle, center_point)
        self.assertAlmostEqual((dist_long - dist_short) / 2, dist_decimal - dist_short)

    def test_DecimalTestAngledLine(self):
        center_point = (0, 0)
        angle = 135
        long = ((200, 200), (210, 210))
        short = ((201, 201), (209, 209))
        decimal = ((200.5, 200.5), (209.5, 209.5))

        dist_long = self.Frame.get_distance(*long, angle, center_point)
        dist_short = self.Frame.get_distance(*short, angle, center_point)
        dist_decimal = self.Frame.get_distance(*decimal, angle, center_point)
        self.assertAlmostEqual((dist_long - dist_short) / 2, dist_decimal - dist_short)

    def test_DecimalTestRandomAngledLine(self):
        center_point = (0, 0)
        angle = 140
        long = ((152, 120), (245, 231))
        short = ((153, 121), (244, 230))
        decimal = ((152.5, 120.5), (244.5, 230.5))

        dist_long = self.Frame.get_distance(*long, angle, center_point)
        dist_short = self.Frame.get_distance(*short, angle, center_point)
        dist_decimal = self.Frame.get_distance(*decimal, angle, center_point)
        self.assertAlmostEqual((dist_long - dist_short) / 2, dist_decimal - dist_short)

    def test_DecimalTestFlatLine(self):
        center_point = (0, 0)
        angle = 90
        long = ((200, 200), (200, 210))
        short = ((200, 201), (200, 209))
        decimal = ((200, 200.7), (200, 209.7))

        dist_long = self.Frame.get_distance(*long, angle, center_point)
        dist_short = self.Frame.get_distance(*short, angle, center_point)
        dist_decimal = self.Frame.get_distance(*decimal, angle, center_point)
        self.assertAlmostEqual(
            (dist_long - dist_short) * 0.7, dist_decimal - dist_short
        )

    def test_DecimalTestAngledLine2(self):
        center_point = (0, 0)
        angle = 135
        long = ((200, 200), (210, 210))
        short = ((201, 201), (209, 209))
        decimal = ((200.3, 200.3), (209.3, 209.3))

        dist_long = self.Frame.get_distance(*long, angle, center_point)
        dist_short = self.Frame.get_distance(*short, angle, center_point)
        dist_decimal = self.Frame.get_distance(*decimal, angle, center_point)
        self.assertAlmostEqual(
            (dist_long - dist_short) * 0.3, dist_decimal - dist_short
        )


if __name__ == "__main__":
    WORKING_DIR = "/home/b-hive/bitbucket/harvesteye/"
    unittest.main()
