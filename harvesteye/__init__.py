"""Main __init__ for package"""
from ._version import get_versions
import re
import os
import sys


def resource_path(relative_path):
    """Get absolute path to resource, works for dev and for PyInstaller"""
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.environ.get("_MEIPASS2", os.path.abspath("."))

    return os.path.join(base_path, relative_path)


def get_comp_ver(version):
    ret = re.search(r"^([0-9]*.[0-9]*)[+]([0-9]*)", version)
    if ret:
        tag = ret.group(1)
        commit = ret.group(2)
        return ".".join(
            [str(tag).split(".")[0], str(int(str(tag).split(".")[1]) + int(commit))]
        )
    else:
        return version


try:
    __version__ = get_comp_ver(get_versions()["version"])
    del get_versions
except IndexError:
    with open(resource_path("version.txt"), "r+") as f:
        __version__ = f.read()
except FileNotFoundError:
    __version__ = "0"
