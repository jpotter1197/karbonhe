"""unit testing for realsense class"""
import unittest
import time
import os
import shutil
from functools import partial
import socketserver
import socket
from threading import Thread

from xml.etree.ElementTree import Element, tostring

from harvesteye.camera import camera
import harvesteye.globals as GLOBALS

from harvesteye.server import handles

# from harvesteye.tracker import AutoTracker
# from harvesteye.segmentation import segmenter, BboxDetection
# from harvesteye.server import RequestHandler
# from harvesteye.server import ClientSocket
# class TestHarvesteyeHandler(RequestHandler):
#     pass
#
# WORKING_DIR = os.getcwd()
# class TestHandleFrameCapture(unittest.TestCase):
#     def setUp(self):
#         GLOBALS.reset()
#         self.test_cam = camera(
#             max_distance=2,
#             repeat=True
#             )
#         test_tracker = AutoTracker(fixed_inc=GLOBALS.fixed_inc)
#         test_segmenter = segmenter(
#             "/home/b-hive/appdata/yolo",
#             inference_res=GLOBALS.inference_res,
#             confidence_thresh=GLOBALS.confidence_thresh,
#             threshold_thresh=GLOBALS.threshold_thresh
#             )
#
#         threads = []
#         GLOBALS.time_between_capture = 1
#         TestHarvesteyeHandler.attact_handle(
#             "Request",
#             partial(
#                 handles.start_threads,
#                 funcs=[
#                 partial(handles.tracking, self.test_cam, test_tracker, GLOBALS),
#                 partial(handles.captured, test_tracker, test_segmenter, BboxDetection, GLOBALS)
#                 ],
#                 threads=threads
#                 )
#             )
#
#         TestHarvesteyeHandler.attact_handle(
#             "stop",
#             partial(
#                 handles.stop,
#                 threads=threads
#                 )
#             )
#
#         HOST, PORT = socket.gethostbyname(socket.gethostname()), 9000
#
#         socketserver.TCPServer.allow_reuse_address = True
#         server = socketserver.TCPServer((HOST, PORT), TestHarvesteyeHandler)
#         self.server_thread = Thread(target=server.serve_forever)
#         self.server_thread.start()
#
#         self.test_client = ClientSocket()
#         self.test_client.connect((HOST, PORT))
#
#     def tearDown(self):
#         self.test_client.send_message(b"stop")
#         time.sleep(1)
#         self.test_client.close()
#         if os.path.isdir(self.directory):
#             shutil.rmtree(self.directory)
#
#     def test_acceptance_1(self):
#         """
#         Given the harvester is on when the harvest has been started by the grower and the belt is moving then I want to capture a sample frame every 2 minutes.
#         """
#         GLOBALS.variety = "acceptance1"
#         self.directory = os.path.join(WORKING_DIR, "frames", GLOBALS.variety) + "/"
#
#         if not os.path.isdir(self.directory):
#             os.makedirs(self.directory)
#
#         self.test_cam = camera(
#             bagfile="/home/b-hive/bitbucket/harvesteye/data/test_videos/test_moving_belt.bag",
#             max_distance=2,
#             repeat=True
#             )
#         self.test_client.send_message(b"Request")
#         start_time = time.time()
#         while True:
#             count = len(os.listdir(self.directory))
#             if time.time() - start_time > 2:
#                 break


class TestHandleFrameCapture(unittest.TestCase):
    def setUp(self):
        self.test_cam = camera(
            bagfile="/home/b-hive/Videos/video0.bag", max_distance=2, repeat=True
        )
        GLOBALS.reset()

    def tearDown(self):
        if os.path.isdir(self.directory) and "max" not in self.directory:
            shutil.rmtree(self.directory)
        GLOBALS.reset()

    def test_frame_capture_1(self):
        GLOBALS.time_between_capture = 1
        GLOBALS.last_capture = time.time()
        GLOBALS.variety = "one"
        self.directory = os.path.join(WORKING_DIR, "frames", GLOBALS.variety)

        start_time = time.time()
        count = 0
        while time.time() - start_time <= 1:
            frames = self.test_cam()
            handles.frame_capture(frames, GLOBALS)
            count = len(os.listdir(self.directory))
        self.assertEqual(count, 1)

    def test_frame_capture_10(self):
        GLOBALS.time_between_capture = 1
        GLOBALS.last_capture = time.time()
        GLOBALS.variety = "ten"
        self.directory = os.path.join(WORKING_DIR, "frames", GLOBALS.variety)

        start_time = time.time()
        count = 0
        while time.time() - start_time <= 11:
            frames = self.test_cam()
            handles.frame_capture(frames, GLOBALS)
            if os.path.exists(self.directory):
                count = len(os.listdir(self.directory))
        self.assertEqual(count, 10)

    def test_frame_capture_max(self):
        GLOBALS.time_between_capture = 1
        GLOBALS.last_capture = time.time()
        GLOBALS.variety = "max"
        self.directory = os.path.join(WORKING_DIR, "frames", GLOBALS.variety)

        start_time = time.time()
        count = 0
        while time.time() - start_time <= 10:
            frames = self.test_cam()
            handles.frame_capture(frames, GLOBALS)
            if os.path.exists(self.directory):
                count = len(os.listdir(self.directory))
        self.assertEqual(count, 250)
        GLOBALS.variety = "acceptance"
        self.directory = os.path.join(WORKING_DIR, "frames", GLOBALS.variety)

    def test_acceptance(self):
        GLOBALS.time_between_capture = 120
        GLOBALS.last_capture = time.time()
        GLOBALS.variety = "acceptance"
        self.directory = os.path.join(WORKING_DIR, "frames", GLOBALS.variety)

        start_time = time.time()
        count = 0
        while time.time() - start_time <= 120:
            frames = self.test_cam()
            handles.frame_capture(frames, GLOBALS)
            if os.path.exists(self.directory):
                count = len(os.listdir(self.directory))
        self.assertEqual(count, 1)


class TestHandleCalibration(unittest.TestCase):
    def setUp(self):
        GLOBALS.reset()
        self.cam = camera(max_distance=GLOBALS.max_distance)

    def tearDown(self):
        GLOBALS.reset()

    def test_set_calibration_max_distance(self):
        objects = [self.cam]
        self.assertEqual(GLOBALS.max_distance, 2)
        self.assertEqual(self.cam.max_distance, 2)
        calib = {
            "Height": 1280,
            "Confidence": 10,
            "ROI1": "0,0,848,480",
            "ROI2": "0,0,0,0",
            "FixedInc": 2,
            "Inference": 832,
            "TimeBetween": 20,
        }
        data = dict_to_xml("save_roi", calib)
        handles.set_calibration(None, data, objects, GLOBALS)
        self.assertEqual(GLOBALS.max_distance, 1.28)
        self.assertEqual(self.cam.max_distance, 1.28)

    def test_get_calibration_max_distance(self):
        self.assertEqual(GLOBALS.max_distance, 2)
        self.assertEqual(self.cam.max_distance, 2)
        ret = handles.get_calibration(None, "", GLOBALS)
        max_distance = ret.decode().split("/")[8]
        self.assertEqual(int(max_distance), 2000)


def dict_to_xml(tag, d):
    elem = Element(tag)
    for key, val in d.items():
        elem.attrib[key] = str(val)
    return elem


if __name__ == "__main__":
    WORKING_DIR = "/home/b-hive/bitbucket/harvesteye/"
    unittest.main()
