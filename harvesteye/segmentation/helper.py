from harvesteye.camera import camera
from harvesteye.camera import FrameFromFile

from harvesteye.tracker import AutoTracker
from harvesteye.segmentation import Yolo, PrecisionEdgeDetection, DetectionsML
from harvesteye import logger

"""Helper Script for tracking"""
import threading
from queue import Empty
import os
import cv2
import keras

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"


def inference_video(cam, segmenter):
    displays = {"inference": None}
    while True:
        frames = cam()
        frames.set_mask([0, 68, 412, 480])
        frames.set_mask([460, 68, 848, 480])
        if frames:
            print(frames.frame_number)
            displays["inference"] = segmenter.inference(frames, PrecisionEdgeDetection)
            displays["rgb"] = frames.colour_frame
            displays["depth"] = frames.depth_frame
            objs = segmenter()
            print(objs)
            for display in displays:
                if displays[display] is not None:
                    cv2.imshow(display, displays[display])
            cv2.waitKey(1)


def inference_frame(frames, segmenter, name=""):
    displays = {segmenter.__class__.__name__: None}
    displays[segmenter.__class__.__name__] = segmenter.inference(
        frames, PrecisionEdgeDetection
    )
    for display in displays:
        if displays[display] is not None:
            cv2.imshow(name + display, displays[display])
    cv2.waitKey(1)


def inference_frame_from_video(cam, segmenter, frame_number=None):
    displays = {"frame": None, "inference": None}
    while True:
        frames = cam()
        displays["frame"] = frames.colour_frame
        if frame_number == frames.frame_number or cv2.waitKey(1) == ord("a"):
            print(frames.frame_number)
            displays["inference"] = segmenter.inference(frames, PrecisionEdgeDetection)
            objs = segmenter(1)
            print(objs)
            for display in displays:
                if displays[display] is not None:
                    cv2.imshow(display, displays[display])
        cv2.waitKey(1)


def inference_video_with_tracking(
    cam,
    segmenter,
    tracker,
    type=PrecisionEdgeDetection,
):
    def tracking():
        """This is constantly feen frames from the camera to the Tracker"""
        while True:
            frames = cam()
            if frames:
                frames.set_mask([0, 68, 412, 480])
                frames.set_mask([460, 68, 848, 480])
                displays["tracker"] = tracker.update(frames)
                displays["rgb"] = frames.colour_frame
                displays["depth"] = frames.depth_frame

    def captured():
        """This will grab any 'captured frames' from the tracker"""
        while True:
            try:
                frames = tracker(10)
                if frames:
                    displays["inference"] = segmenter.inference(frames, type)
            except Empty:
                pass

    displays = {"rgb": None, "depth": None, "tracker": None, "inference": None}
    trackerthread = threading.Thread(target=tracking)
    trackerthread.start()
    capturethread = threading.Thread(target=captured)
    capturethread.start()
    while True:
        for display in displays:
            if displays[display] is not None:
                cv2.imshow(display, displays[display])
        cv2.waitKey(1)


if __name__ == "__main__":
    logger.get_logger("Segmentation").setLevel(20)

    DetectionsML.HEIGHT_MODEL = keras.models.load_model(
        "/mnt/data/appdata/depth_model.h5"
    )
    DetectionsML.SIZEBAND_MODEL = keras.models.load_model(
        "/mnt/data/appdata/sizeband_model.h5"
    )
    DetectionsML.WEIGHT_MODEL = keras.models.load_model(
        "/mnt/data/appdata/weight_model.h5"
    )
    seg = Yolo("/mnt/data/appdata/yolo", confidence_thresh=0.1, threshold_thresh=0.8)
    # seg.detections = DetectionsML
    cam = camera()  # bagfile="/home/b-hive/HE004-video.bag", repeat=True)
    tracker = AutoTracker()
    print("This is a test print to fix pipeline")
    while True:
        inference_frame(cam(), seg)
    # inference_frame()

    # filename = "20210430_123327.bag"
    # direc = "/media/b-hive/Elements1/Grimme_30_04_2020/Normal/"
    # filename= "video0.bag"
    # direc = "/home/b-hive/Videos/"

    # cam = camera(bagfile=direc + filename, repeat=False, realtime=False)
    # inference_video_with_tracking(cam, seg, tracker)
    # folder_path =""
    # CAMS = cameras(folder_path, repeat=True)
    # FRAMES = CAMS(True)
    # YOLO = Yolo(
    # "/home/b-hive/appdata/potato",
    # confidence_thresh=0.8,
    # threshold_thresh=0.9
    # )

    # size_frames(FRAMES, YOLO,output=folder_path, type=BboxDetection)
    # size_frames(FRAMES, YOLO,output=folder_path, type=EdgeDetection)
    # size_frames(FRAMES, YOLO,output=folder_path, type=PrecisionEdgeDetection)
