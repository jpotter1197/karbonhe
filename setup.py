from setuptools import setup

with open("requirements.txt") as f:
    required = f.read().splitlines()

setup(
    name="harvesteye",
    version=2.00,
    packages=[
        "harvesteye",
        "harvesteye.camera",
        "harvesteye.tracker",
        "harvesteye.segmentation",
        "harvesteye.globals",
        "harvesteye.gps",
        "harvesteye.server",
        "harvesteye.utils",
        "harvesteye.third_party_models",
        "harvesteye.extras",
    ],
    install_requires=required,
)
