import socket
from queue import Queue


class ClientSocket(socket.socket):
    segment_size = 1024

    def __init__(self, *args, **kwargs):
        super(ClientSocket, self).__init__(*args, **kwargs)
        self._queue = Queue(maxsize=256)

    def read_message(self):
        """Adds a message to the queue"""
        try:
            message_length = self.convert_size_from_byte(self.recv(4))
            return self._receive(message_length)
        except IndexError:
            return None

    def _receive(self, length):
        """Recieves a messages based on length of bytes"""
        data = [None] * length
        offset = 0

        full_reads = length // self.segment_size
        partial_read = length % self.segment_size
        # perform required number of recieve operations and update offset
        # value in the data array
        # print(datetime.now().time())
        for _ in range(full_reads):
            # if (full_reads - x) % 100 == 0:
            # print("full reads:" + str(full_reads - x))
            data[offset : offset + self.segment_size] = self.recv(
                self.segment_size
            ).decode("utf-8")
            offset += self.segment_size
        # perform final partial read
        data[offset : offset + partial_read] = self.recv(partial_read).decode("utf-8")
        # print(datetime.now().time())
        return bytes("".join(data), encoding="utf8")

    def convert_size_from_byte(self, v_size_info):
        """converts 4 byte header into byte size information"""
        messagesize = 0
        messagesize += v_size_info[3]
        messagesize += (v_size_info[2]) << 8
        messagesize += (v_size_info[1]) << 16
        messagesize += (v_size_info[0]) << 24
        return messagesize

    def send_message(self, message):
        """sends a messages with a header of the size information"""
        try:
            size_info = len(message).to_bytes(4, byteorder="little")
            data = size_info + message
            self.sendall(data)
        except TypeError:
            pass


if __name__ == "__main__":
    print("dwadw")
