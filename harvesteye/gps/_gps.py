from harvesteye.gps.abstracts import GPS
from harvesteye.gps._messages import messages
from serial.tools import list_ports
from harvesteye import logger

NSLogger = logger.get_logger("GPS")
ErrLogger = logger.get_logger("error")
EventLogger = logger.get_logger("EVENT")


class Prolific(GPS):
    MESSAGES = messages

    def __init__(self, port, baud=4800):
        super().__init__(port, baud)


class Mock(GPS):
    def __init__(self):
        self.lat = 0
        self.long = 0


def gps():
    """used to return the correct writer"""
    ports = list_ports.comports()
    for cls in GPS.__subclasses__():
        for port in ports:
            if port.manufacturer:
                if cls.is_gps(port.manufacturer):
                    NSLogger.info(
                        "Found Device %s, returning device interface %s",
                        port.manufacturer,
                        cls.__name__,
                    )
                    return cls(port.device)
    NSLogger.warning("No GPS Found, Returning Mock GPS")
    return Mock()
