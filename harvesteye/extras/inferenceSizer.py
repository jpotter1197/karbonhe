import argparse
import os
import cv2
from harvesteye.camera import cameras
from harvesteye.segmentation import Yolo, detection_factory


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise argparse.ArgumentTypeError("Boolean value expected.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--bagfiles", default=None, help="Path to bagfile or folder of bags"
    )
    parser.add_argument("--model", help="Path to model files")
    parser.add_argument("--conf", default=0.8, help="confidence_thresh of inference")
    parser.add_argument("--type", default="Precision", help="type of detection")
    parser.add_argument("--output", default=None, help="output of csv files")
    parser.add_argument("--show", type=str2bool, default=False)
    parser.add_argument(
        "--single",
        type=str2bool,
        default=True,
        help="If the path given is a single frames bagfile",
    )
    args = parser.parse_args()

    # args.bagfiles = "/media/b-hive/Elements1/gigatest_results/800-2/"
    # args.model = "/home/b-hive/appdata/potato"
    # args.show = True

    if args.model is None:
        raise ValueError("please parse --model")
    if args.bagfiles is None or not os.path.isdir(args.bagfiles):
        raise ValueError("Please parse directory of bagfiles --bagfiles")
    if args.output is None:
        args.output = args.bagfiles

    file_path = os.path.join(
        args.output,
        detection_factory(args.type).__name__
        + "-"
        + os.path.basename(args.output)
        + ".csv",
    )

    cams = cameras(args.bagfiles, repeat=True)
    frames = cams(True)
    seg = Yolo(args.model, confidence_thresh=0.8, threshold_thresh=0.9)
    for frame in frames:

        inf = seg.inference(frame, detection_factory(args.type))
        key = 0
        if args.show:
            cv2.imshow("inf", inf)
            key = cv2.waitKey(0)
        objs = seg()
        for obj in objs:
            if args.show and key in range(ord("0"), ord("9")):
                if obj.ID is not int(chr(key)) and args.show:
                    continue
            print(
                "frame {}, {} object {}: size: {}".format(
                    frame.frame_number, objs.__class__.__name__, obj.ID, str(obj)
                )
            )
            if args.output:
                with open(file_path, "a") as file:
                    file.write(str(frame.potato_number) + "," + str(obj) + "\n")
