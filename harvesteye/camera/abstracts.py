"""
Abstract class for any camera class.py:
camera functionality
"""
# pylint: disable=import-error
# pylint: disable=no-name-in-module
__author__ = "Craig Cooper"
__copyright__ = "Copyright 2020, B-hive Innovations"
from abc import ABC, abstractmethod, abstractproperty

import numpy as np
import cv2

from harvesteye import logger

NSLogger = logger.get_logger("Camera")
ErrLogger = logger.get_logger("error")


class DistanceError(Exception):
    pass


class Frame(ABC):
    # pylint: disable=attribute-defined-outside-init
    """
    abstract methods and properties the is coupled with the camera being used
    IE Each camera should have its related Frame to deal
    A Frame class can be created to deal with over situations,
    for example reading from files
    NOTE: if the camera returns there own object type to wrap the frames,
    this object should \
    inherit from that class too.
    WARNING: metaclass cconflict will happen,
    see harvesteye.camera._realsense.FrameMeta to see
    how the the realsense frame is handled.
    """

    @abstractproperty
    def colour_frame(self):
        """
        This method is basicly a facade for creating a colour image
        args: None
        return: numpy ndarray of type np.uint8 with the shape of the image.
        """
        pass

    @abstractproperty
    def depth_frame(self):
        """
        This method is basicly a facade for depth frame, USED FOR DISPLAY ONLY
        args: None
        return: numpy ndarray of type np.uint8 with the shape of the image.
        """
        pass

    @abstractmethod
    def get_distance(self, point1, point2):
        """
        This method is will get the distance in mm from
        two points in the image.
        args: point1, point2
        return: float, this is in units of mm
        """
        pass

    @abstractmethod
    def depth(self):
        """
        This method will be used to get depth at a cente pixel coord
        """
        pass

    def save_colour(self, save_path, filename):
        try:
            # Save colour frame and colourized depth frame
            NSLogger.debug("Saving %s to %s", filename, save_path)
            cv2.imwrite(save_path + filename + ".png", self.colour_frame)
        except Exception as error:
            NSLogger.exception(
                "Errored while saving image %s to %s", filename, save_path
            )
            ErrLogger.error("Errored while saving image")
            raise error

    def save_depth(self, save_path, filename):
        # Save colour frame and colourized depth frame to aid with bug fixing.
        try:
            point_map = []
            num_rows = self.colour_frame.shape[0]
            num_cols = self.colour_frame.shape[1]
            for row in range(0, num_rows, 1):
                for col in range(0, num_cols, 1):
                    depth = self.depth((col, row))
                    point_map.append(depth)
            point_map = np.array(point_map)
            point_map = np.reshape(
                point_map, (self.colour_frame.shape[1], self.colour_frame.shape[0], 3)
            )
            np.save(save_path + filename + ".npy", point_map)
        except Exception as error:
            NSLogger.exception(
                "Errored while saving image %s to %s", filename, save_path
            )
            ErrLogger.error("Errored while saving image")
            raise error

    def save(self, save_path, filename):
        self.save_colour(save_path, filename)
        self.save_depth(save_path, filename)

    def __setattr__(self, name, value):
        """
        This method will set attrubutes to the frame allowing
        for camera settings to be passed \
        incase the frame needs said settings to do calculation
        args: name, value
        return: None
        """
        self.__dict__[name] = value

    def set_mask(self, key):
        """
        This method will create a boolen mask,
        set the region-of-interest based {key[x1,x2,y1,y2]}.
        args: name, value
        return: None
        """
        try:
            if not hasattr(self, "_mask"):
                NSLogger.debug("No _mask attr, Creating")
                self._mask = np.full(
                    (self.colour_frame.shape[0], self.colour_frame.shape[1]),
                    False,
                    dtype=bool,
                )
            if not hasattr(self, "_parent"):
                self._parent = key
            else:
                maxroi = np.amax(
                    [key, self._parent], axis=0
                )  # find the max values of the cols
                minroi = np.amin(
                    [key, self._parent], axis=0
                )  # find the min value of the cols
                self._parent = [minroi[0], minroi[1], maxroi[2], maxroi[3]]
            if not hasattr(self, "regions"):
                self.regions = []
            self.regions.append(key)
            NSLogger.debug("Setting mask %s on image", key)
            self._mask[key[1] : key[3], key[0] : key[2]] = True
        except Exception as error:
            NSLogger.exception("Errored while setting mask %s, %s", key, self.__dict__)
            ErrLogger.error("Errored while setting mask")
            raise error

    @property
    def shape(self):
        """
        This method sends back shape of both colour and depth.
        This should never need to change if abstract methods
        are created properly.
        args: name, value
        return: shape of colour image and depth image in a tuple of tuples
        """
        return self.colour_frame.shape

    @property
    def masked(self):
        """
        This method sends back the masked colour image
        if there is a _mask attr.
        args: None
        return: masked colour numpy.ndarray[np.uint8]
        """
        try:
            if not hasattr(self, "_mask"):
                NSLogger.debug("No _mask attr, return colour frame")
                return self.colour_frame
            NSLogger.debug("Mask found: returning masked image")
            return cv2.bitwise_or(
                self.colour_frame,
                self.colour_frame,
                mask=1 * self._mask.astype(np.uint8),
            )
        except Exception as error:
            NSLogger.exception(
                "Errored while returning masked image, %s", self.__dict__
            )
            ErrLogger.error("Errored while returning masked image")
            raise error

    @property
    def cropped_masked(self):
        """
        This method sends back the cropped masked colour image
         if there is a _mask attr.
        Notice the there are no inputs the cropped x1,x2,y1,y2
        will be calculated based on mask
        args: None
        return: masked colour numpy.ndarray[np.uint8]
        """
        try:
            if not hasattr(self, "_mask"):
                NSLogger.debug("No _mask attr, return colour frame")
                return self.colour_frame
            NSLogger.debug("Mask found: cropping")
            return self.masked[
                self._parent[1] : self._parent[3], self._parent[0] : self._parent[2]
            ]
        except Exception as error:
            NSLogger.exception("Errored while cropping masked image, %s", self.__dict__)
            ErrLogger.error("Errored while cropping masked image")
            raise error

    def track(self, scale=0.25):
        """
        Cleans the provided image ready for tracking

        * Converts to greyscale if colour
        * resizes it down to :py:attr:`self.scale`

        :args img: the frame to track
        :return: the cleaned image
        """
        try:
            NSLogger.debug("Cleaning image for tracking, scale: %s", scale)
            img = self.cropped_masked
            if img.ndim == 3:
                img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            img = cv2.resize(np.array(img), None, (0, 0), fx=scale, fy=scale)
            return img
        except Exception as error:
            NSLogger.exception(
                "Errored while cleaing image for tracking, %s", self.__dict__
            )
            ErrLogger.error("Errored while cleaing image for tracking")
            raise error

    @property
    def inverted_masked(self):
        """
        inverts the mask and sends back said masked colour frame
        args: None
        return masked colour frame.
        """
        try:
            if not hasattr(self, "_mask"):
                NSLogger.debug("No _mask attr, return colour frame")
                return self.colour_frame
            NSLogger.debug("Mask found: returning inverted masked image")
            mask = np.invert(self._mask)
            return cv2.bitwise_or(
                self.colour_frame, self.colour_frame, mask=1 * mask.astype(np.uint8)
            )
        except Exception as error:
            NSLogger.exception(
                "Errored while returning inverted-masked imaged, %s", self.__dict__
            )
            ErrLogger.error("Errored while returning inverted-masked imaged")
            raise error

    def reset_mask(self):
        NSLogger.debug("Resetting Mask")
        if hasattr(self, "_mask"):
            delattr(self, "_mask")

    def crop(self, key, padding=0):
        NSLogger.debug("cropping..")
        key = [key[0] - padding, key[1] - padding, key[2] + padding, key[3] + padding]
        return self.colour_frame[key[1] : key[3], key[0] : key[2]]


class Borg(object):
    # pylint: disable=too-few-public-methods
    # pylint: disable=no-self-use
    """
    Borg Design pattern used for some camera
    """
    shared_state = {}

    def __init__(self):
        self.__dict__ = self.shared_state

    def _reset(self):
        Camera.shared_state = {}


class Camera(ABC):
    """
    Camera abstract used to create new camera types
    which must have these functions.
    minium attrs that the camera must have.
    """

    _defaults = {"xres": 848, "yres": 480, "fps": 30}
    """
    parents functions
    This should never need to change these functions
    if abstract methods are created properly.
    """

    def __init__(self, **kwargs):
        """
        set default values and then start the camera pipeline
        args: kwargs
        return: None
        """
        self.start(kwargs)

    def _reset(self):
        """
        args: None
        return: None
        """
        self.stop()

    @abstractmethod
    def start(self, kwargs):
        """
        This Method is used to update kwargs and start the camera pipeline.
        cam = Camera()
        creates new camera with shared vars of another is exists
        cam = Camera(kwargs) # changes settings for all cameras
        All Cameras should pull of the same pipeline
        args: kwargs
        return: None
        """
        pass

    @abstractmethod
    def stop(self):
        """
        This method is used to shutdown the pipeline/camera
        args: None
        return: None
        """
        pass

    @abstractmethod
    def __call__(self):
        """
        This method is used to camera is called a Frame
         should be returned basicilly get_frame().
        args: None
        return: Frame
        NOTE: Frame object needs to be created to handle the camera properties
        """
        pass

    @classmethod
    def is_camera(cls, mtype, kwargs):
        return (
            cls.__name__.lower() in mtype.lower()
            and kwargs.keys() <= cls._defaults.keys()
        )

    @classmethod
    def uses_file(cls, kwargs):
        return False
