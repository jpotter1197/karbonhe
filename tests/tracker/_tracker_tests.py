import unittest
import numpy as np
from harvesteye.tracker import AutoTracker
from harvesteye.camera import Realsense


class TestUpTracker(unittest.TestCase):
    def setUp(self):
        self.cam = Realsense(bagfile="/home/b-hive/video0.bag")
        self.tracker = AutoTracker()

    def tearDown(self):
        pass

    def test_update(self):
        _next = self.tracker._next
        position = None
        self.assertEqual(_next is None, True)
        for i in range(0, 50):
            frames = self.cam()
            self.tracker.update(frames)
            if position is None:
                position = self.tracker.current_position
            _next = self.tracker._next
            self.assertEqual(_next is None, False)
            np.testing.assert_array_equal(frames.track(), _next)
        self.assertEqual(self.tracker.current_position < position, True)

    def test_call(self):
        _next = self.tracker._next
        position = None
        self.assertEqual(_next is None, True)
        while True:
            frames = self.cam()
            self.tracker.update(frames)
            if position is None:
                position = self.tracker.current_position
            _next = self.tracker._next
            self.assertEqual(_next is None, False)
            np.testing.assert_array_equal(frames.track(), _next)
            if self.tracker.current_position < position:
                break
        self.assertEqual(self.tracker.current_position < position, True)
        frames = self.tracker()
        np.testing.assert_array_equal(frames.track(), _next)


if __name__ == "__main__":
    unittest.main()
