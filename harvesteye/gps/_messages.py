from harvesteye.gps.abstracts import Decoder
from harvesteye.gps._utils import gpgga_to_decimal_degrees

from harvesteye import logger

NSLogger = logger.get_logger("GPS")
EventLogger = logger.get_logger("EVENT")
ErrLogger = logger.get_logger("error")


class GPGGA(Decoder):
    def decode(self, message):
        self.satellites = int(message[6])
        if self.satellites >= 3:
            latlon = (message[1], message[2], message[3], message[4])
            self.lat, self.long = gpgga_to_decimal_degrees(latlon)
        else:
            self.lat, self.long = (0, 0)
        self.__dict__[self.__class__.__name__] = message

        EventLogger.info(
            "[EVENT:GPSPosition],"
            + str(
                {
                    "latitude": self.lat,
                    "longitude": self.long,
                    "satellites": self.satellites,
                }
            )
        )
        return self.__dict__


class GNGGA(Decoder):
    def decode(self, message):
        self.satellites = int(message[6])
        self.fixtype = self.message[5]
        if self.satellites >= 3:
            latlon = (message[1], message[2], message[3], message[4])
            self.lat, self.long = gpgga_to_decimal_degrees(latlon)
        else:
            self.lat, self.long = (0, 0)
        self.__dict__[self.__class__.__name__] = message
        EventLogger.info(
            "[EVENT:GPSPosition],"
            + str(
                {
                    "latitude": self.lat,
                    "longitude": self.long,
                    "satellites": self.satellites,
                }
            )
        )
        return self.__dict__


class GNVTG(Decoder):
    def decode(self, message):
        self.current_direction = self.message[0]
        self.current_speed = self.message[6]
        self.__dict__[self.__class__.__name__] = message
        return self.__dict__


class UNKNOWN(Decoder):
    def decode(self, message):
        self.__dict__[message[0][1:]] = message
        return self.__dict__


def messages(bmessage):
    """used to return the correct writer"""
    data = bmessage.split(",")
    for cls in Decoder.__subclasses__():
        if cls.is_decoder(data[0]):
            ret = cls().decode(data[1:])
            NSLogger.debug("Decoded %s Message: %s", cls.__name__, ret)
            return ret
    return UNKNOWN().decode(data)
