from abc import ABC, abstractmethod
from threading import Thread
import serial
import time
from harvesteye import logger

EventLogger = logger.get_logger("EVENT")


class Decoder(ABC):
    @abstractmethod
    def decode(self, bmessage):
        pass

    def __setattr__(self, name, value):
        self.__dict__[name] = value

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return str(self.__dict__)

    @classmethod
    def is_decoder(cls, mtype):
        return cls.__name__ in mtype


class Borg(object):
    # pylint: disable=too-few-public-methods
    # pylint: disable=no-self-use
    """
    Borg Design pattern used for the camera
    """
    shared_state = {}

    def __init__(self):
        self.__dict__ = self.shared_state


class GPS(ABC, Borg):
    MESSAGES = None
    _defaults = {
        "_status": False,
    }

    def __init__(self, port, baud):
        Borg.__init__(self)
        self.__dict__.update(self._defaults)
        self._stream = serial.Serial(port, baud)
        self.start()

    def __getattr__(self, name):
        return self.__dict__[name]

    def start(self):
        if not self._status:
            self._status = True
            process = Thread(target=self._reader)
            process.start()

    def stop(self):
        if self._status:
            self.__status = False

    def _reader(self):
        EventLogger.info("[EVENT:GPSStatus],{'STATUS':1}")
        self._readline()
        while self._status:
            time.sleep(0.25)
            self._readline()

    def _readline(self):
        try:
            self.__dict__.update(
                self.__class__.MESSAGES(self._stream.readline().decode())
            )
        except UnicodeDecodeError:
            EventLogger.info("[EVENT:GPSStatus],{'STATUS':0}")
            pass
        except serial.SerialException:
            EventLogger.info("[EVENT:GPSStatus],{'STATUS':0}")
            pass

    def __repr__(self):
        return str(self.__dict__)

    @classmethod
    def is_gps(cls, mtype):
        return cls.__name__ in mtype
