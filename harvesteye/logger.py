"""
A logging utility class::

    import harvesteye_logger as logger
    log = logger.get_logger("frame_processor")

or::

    log = logger.get_logger(__name__)

Allows easy logging level functionality::

    log.info("Information text")
    log.error("Run!")
    etc

Writes to: log/harvesteye-YYYYMMDD-HHMMSS.log
"""

import logging
import logging.handlers
import os
import stat
import sys
from logging.handlers import TimedRotatingFileHandler


FORMATTER = logging.Formatter(
    "%(asctime)s — %(name)s — %(levelname)s:[%(filename)"
    's:%(lineno)s - %(funcName)s() — %(message)s"]'
)
BASE_PATH = os.path.dirname(os.path.join(os.path.abspath(os.path.dirname(__file__))))
LOG_DIR = os.path.join(BASE_PATH, "log")
print("saving logs to {}".format(LOG_DIR))
if not os.path.exists(LOG_DIR):
    os.mkdir(LOG_DIR)


def get_console_handler():
    """
    Returns a handler to the console (stdout) which spits out
    entries that are being written to the log
    It is hard coded here to only display ERROR level and above

    :return: a streamhandler
    """
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setLevel((logging.INFO))
    console_handler.setFormatter(FORMATTER)
    return console_handler


def get_file_handler(logger_name):
    """
    Returns a handler to the file logger which logs entries to
    file that are being written to the log
    At midnight, the filename is rotated

    :return: a streamhandler
    """
    try:
        file = os.path.join(LOG_DIR, logger_name + ".log")
        file_handler = TimedRotatingFileHandler(file, when="midnight")
        file_handler.setFormatter(FORMATTER)
        file_handler.setLevel(logging.INFO)
        return file_handler
    except PermissionError as error:
        file = error.filename
        os.chmod(file, stat.S_IRWXG)
    except Exception as error:
        raise error


def get_logger(logger_name):
    """
    The main "factory" method that returns a logger with the name specified.
    Used at the top of a module passing in the name of the module

    This will then write to the logs with the logger name t
    o identify the source

    :param logger_name: Usually the class/module name
    :return: logger object that writes to the console and to the log file
    """
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.INFO)
    # better to have too much log than not enough
    if logger.hasHandlers():
        logger.handlers.clear()
    logger.addHandler(get_console_handler())
    logger.addHandler(get_file_handler(logger_name))
    """
    with this pattern, it's rarely necessary
    to propagate the error up to parent
    """
    # logger.propagate = False
    return logger


def shutdown():
    """
    Shuts down the logging

    """
    logging.shutdown()


if __name__ == "__main__":
    LOG = get_logger(__name__)
    try:
        LOG.debug("Testing")
    #  exit(main())
    except Exception as error:
        LOG.exception("Exception in main()")
        raise error
