"""Tracker module holding different types of tracker children"""
# pylint: disable=import-error
# pylint: disable=no-name-in-module
# pylint: disable=superfluous-parens
from functools import partial
from queue import Empty

from harvesteye.tracker.abstracts import Tracker
from harvesteye.tracker._restrictions import (
    CalcNegativeRestrictedFlow,
    CalcPositiveRestrictedFlow,
    CalcFreeFlow,
)
from harvesteye import logger

NSLogger = logger.get_logger("Tracker")
ErrLogger = logger.get_logger("error")


class UpTracker(Tracker):
    """used to tracker the images upwards"""

    # pylint: disable=attribute-defined-outside-init
    # pylint: disable=access-member-before-definition
    RESTRICTION = CalcNegativeRestrictedFlow()

    def condition(self, frames):
        """main thread"""
        self.reset_pos = self.win_height - (self.win_height / 6)
        try:
            NSLogger.debug(
                "Current position: %s, trigger point: %s",
                self.current_position,
                (self.win_height / 6),
            )
            if self.current_position >= self.reset_pos:
                NSLogger.info("tracker moved, returned image.")
                self.current_position = 0
                self.images.put(frames)
        except Exception as error:
            raise error


class DownTracker(Tracker):
    """used to tracker the image downwards"""

    # pylint: disable=attribute-defined-outside-init
    # pylint: disable=access-member-before-definition
    RESTRICTION = CalcPositiveRestrictedFlow()

    def condition(self, frames):
        """main thread"""
        self.reset_pos = 0 + (self.win_height / 6)
        try:
            NSLogger.debug(
                "Current position: %s, trigger point: %s",
                self.current_position,
                (self.win_height / 6),
            )
            if self.current_position <= self.reset_pos:
                NSLogger.info("tracker moved, returned image.")
                self.current_position = self.win_height
                self.images.put(frames)
        except Exception as error:
            raise error


class AutoTracker(UpTracker, DownTracker):
    """
    used to automatically detereming what tracker
    and restrictions should be used
    """

    # pylint: disable=attribute-defined-outside-init
    # pylint: disable=access-member-before-definition
    # pylint: disable=method-hidden
    RESTRICTION = CalcFreeFlow()

    def __call__(self, timeout=None):
        """object call"""
        try:
            return self.images.get(timeout=timeout)
        except Empty:
            NSLogger.info("reseting condition and restriction")
            self.condition = partial(AutoTracker.condition, self)
            AutoTracker.RESTRICTION = CalcFreeFlow()
            raise

    def condition(self, frames):
        """main thread"""
        try:
            NSLogger.debug(
                "Current position: %s, trigger point: %s",
                self.current_position,
                (0, self.win_height),
            )
            if self.current_position <= 0:
                NSLogger.info("Tracked Downwards, swaping to Down tracker condition")
                self.condition = partial(DownTracker.condition, self)
                AutoTracker.RESTRICTION = DownTracker.RESTRICTION
            elif self.current_position >= self.win_height:
                NSLogger.info("Tracked Upwards, swaping to up tracker condition")
                self.condition = partial(UpTracker.condition, self)
                AutoTracker.RESTRICTION = UpTracker.RESTRICTION
        except Exception as error:
            raise error
