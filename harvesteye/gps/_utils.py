def gpgga_to_decimal_degrees(latlon):
    """does a thing"""
    lat = _dms_to_dd(latlon[:2])
    lon = _dms_to_dd(latlon[2:])
    return (lat, lon)


def _dms_to_dd(dms):
    """Does another thing"""
    try:
        coord = dms[0]
        hemi = dms[1]
        brk = coord.find(".") - 2
        if brk < 0:
            brk = 0
        minutes = coord[brk:]
        degrees = coord[:brk]
        new_dms = float(degrees) + float(minutes) / 60
        if hemi in ("W", "S"):
            new_dms = -1 * new_dms
        return new_dms
    except ValueError:
        # print("Unable to receive coordinate")
        return 0
