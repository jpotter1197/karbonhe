from harvesteye.segmentation._yolo import Yolo
from harvesteye.segmentation.abstracts import Detection, Detections, Segmenter
from harvesteye.segmentation._detections import DetectionsML
from harvesteye.segmentation._detections import (
    BboxDetection,
    EdgeDetection,
    PolyDetection,
    PrecisionEdgeDetection,
)

from functools import partial
from pathlib import Path
from harvesteye import logger
import numpy as np

NSLogger = logger.get_logger("Segmentation")
ErrLogger = logger.get_logger("error")


def attach_gps_to_detection(GPS, Detection):
    NSLogger.info("Attaching %s to %s", GPS.__class__.__name__, Detection.__name__)

    class Detection_with_GPS(Detection):
        def __init__(self, gps, **kwargs):
            super().__init__(**kwargs)
            self.lat = gps.lat
            self.long = gps.long

        def __str__(self):
            if None in self.dimensions or np.nan in self.dimensions:
                return ""
            else:
                line = [x for x in self.dimensions] + [
                    self.lat,
                    self.long,
                    self.timestamp,
                ]
                return ", ".join(map(str, line))

    return partial(Detection_with_GPS, gps=GPS)


class UnsupportedModelFiles(Exception):
    pass


class DetectionTypeNotFound(Exception):
    pass


def segmenter(folder_path, **kwargs):
    NSLogger.debug("Calling Segmenter factory to determine correct class")
    path = Path(folder_path).glob("**/*")
    files_in_folder = [x for x in path if x.is_file()]
    files = {}
    for file in files_in_folder:
        files[file.suffix[1:]] = file
    for cls in Segmenter.__subclasses__():
        if cls.is_segmenter(list(files.keys())):
            return cls(path=folder_path, **kwargs)
    raise UnsupportedModelFiles


def detection_factory(type, clss=Detection):
    NSLogger.debug("Calling detection factory to determine correct class")
    for cls in clss.__subclasses__():
        if cls.is_detection(type):
            return cls
        else:
            return detection_factory(type, clss=cls)
    raise DetectionTypeNotFound
