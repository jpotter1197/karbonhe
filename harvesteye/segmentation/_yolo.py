"""Module for Yolo object detection."""
import time
import cv2
import numpy as np
from harvesteye.segmentation.abstracts import Segmenter
from harvesteye.segmentation._detections import BboxDetection
from harvesteye import logger

NSLogger = logger.get_logger("Segmentation")
ErrLogger = logger.get_logger("error")


class Yolo(Segmenter):
    """
    Loads in Yolo model from a config and a weights file.
    Detects objects in a given frame.
    """

    def setup(self):
        try:
            NSLogger.info("Building Darknet")
            self._net = cv2.dnn.readNetFromDarknet(str(self.cfg), str(self.weights))
            layer_names = self._net.getLayerNames()
            self._layer_names = []
            for i in self._net.getUnconnectedOutLayers():
                self._layer_names.append(layer_names[i[0] - 1])
        except Exception as error:
            NSLogger.exception("YOLO: %s Errored while building darknet", self.__dict__)
            ErrLogger.error("YOLO: Errored while building darknet")
            raise error

    def inference(self, frame, detection_type=BboxDetection, swap=True):
        try:
            image = np.array(frame.masked)
            orignal_height, orignal_width, _ = image.shape
            startx = time.time()
            blob = cv2.dnn.blobFromImage(
                image,
                1 / 255.0,
                (self.inference_res, self.inference_res),
                swapRB=swap,
                crop=False,
            )
            self._net.setInput(blob)
            layer_outputs = self._net.forward(self._layer_names)
            if NSLogger.level == 10:
                disp = np.array(frame.masked)
            else:
                disp = np.array(frame.colour_frame)
            detections = self.detections(frame=frame)
            id = 0
            for output in layer_outputs:
                for det in output:
                    scores = det[4:]
                    class_id = np.argmax(scores)
                    confidence = scores[class_id]
                    if confidence > self.confidence_thresh:
                        box = det[0:4] * np.array(
                            [
                                orignal_width,
                                orignal_height,
                                orignal_width,
                                orignal_height,
                            ]
                        )
                        try:
                            detection = detection_type(
                                id=id,
                                bbox=box.astype("int"),
                                confidence=float(confidence),
                                class_id=class_id,
                                shape=disp.shape,
                                frame=frame,
                            )
                            id += 1
                            detections.append(detection)
                        except ValueError:
                            NSLogger.debug("detection failed to init, moving to next")
            NSLogger.debug("%s detections created", len(detections))
            detections = self.filter(detections)
            NSLogger.info(
                "inference speed: %s, fps found %s detections",
                1.0 / (time.time() - startx),
                len(detections),
            )
            detections.calculate_dimensions()
            NSLogger.debug("added %s Objects to queue", len(detections))
            self.object_queue.put(detections)
            return detections.draw()
        except Exception as error:
            NSLogger.exception("YOLO: %s Errored while inferencing", self.__dict__)
            ErrLogger.error("YOLO: Errored while inferencing")
            raise error

    def filter(self, objs):
        """Used to filter out unwanted objects."""
        try:
            idxs = cv2.dnn.NMSBoxes(
                [obj.bbox for obj in objs],
                [obj.confidence for obj in objs],
                self.confidence_thresh,
                self.threshold_thresh,
            )
            filtered = self.detections(frame=objs.frame)
            # ensure at least one detection exists
            if len(idxs) > 0:
                # loop over the indexes we are keeping
                for i in idxs.flatten():
                    # extract the bounding box coordinates
                    obj = objs[i]

                    filtered.append(obj)
            NSLogger.debug("%s objects removed by filter", len(objs) - len(filtered))
            return filtered
        except Exception as error:
            NSLogger.exception("YOLO: %s Errored while filtering", self.__dict__)
            ErrLogger.error("YOLO: Errored while filtering")
            raise error

    @classmethod
    def is_segmenter(cls, model_files):
        required_files = ["cfg", "weights"]
        required_files.sort()
        model_files.sort()
        return required_files == model_files
