import cv2
import numpy as np
import os
from harvesteye.camera import camera
from harvesteye.camera.helper import display_frame
from pathlib import Path

import argparse


class limitedlist(list):
    def __init__(self, max=25):
        self.idx = 0
        self.max = max

    def append(self, value):
        if len(self) >= self.max:
            self.pop(0)
        else:
            self.idx += 1
        self[len(self) :] = [value]

    def next(self):
        if self.idx < len(self):
            self.idx += 1
        try:
            return self[self.idx - 1]
        except IndexError:
            return None

    def previous(self):
        if self.idx - 1 > 0:
            self.idx -= 1
        try:
            return self[self.idx - 1]
        except IndexError:
            return None

    def current(self):
        try:
            return self[self.idx - 1]
        except IndexError:
            return None

    @property
    def is_current(self):
        return self.idx == len(self)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--bagfile", default=None, help="file path to a .bag file")
    parser.add_argument(
        "--output", default=None, help="folder path to output single frame bag files"
    )
    parser.add_argument(
        "--remember",
        default=120,
        help="How many frames do you want" "to remember in order to rewind the video",
    )
    args = parser.parse_args()
    if args.bagfile is None:
        raise ValueError("bagfile path not provided")
    if args.output is None:
        root = os.path.dirname(args.bagfile)
        folder = os.path.basename(args.bagfile[:-4])
        args.output = os.path.join(root, folder)
    if not os.path.exists(args.output):
        os.mkdir(args.output)
        last = None
        potato_number = 1
    else:
        pths = Path(args.output).glob("**/*{}".format(".bag"))
        nums = np.array(
            [[str(pth).split("_")[-1][:-4], str(pth).split("_")[-3]] for pth in pths]
        )
        try:
            last = max(nums[:, 0].astype(np.int))
            potato_number = max(nums[:, 1].astype(np.int)) + 1
            print(last)
        except IndexError:
            last = None
            potato_number = 1

    cam = camera(bagfile=args.bagfile, realtime=False)
    frames = limitedlist()
    if last:
        frame = cam()
        cam.realtime = True
        while frame.frame_number <= int(last):
            frame = cam()
            # display_frame(frame)
            frames.append(frame)
        cam.realtime = False
    else:
        frames.append(cam())
    while True:
        key = cv2.waitKey(0)
        if key == ord("a"):
            if frames.is_current:
                cam.realtime = True
                frames.append(cam())
            display_frame(frames.next())
        elif key == ord("s"):
            frames.current().save(
                args.output,
                filename="{}_{}_".format(
                    os.path.basename(args.bagfile)[:-4], potato_number
                ),
            )
            print("Saved Bag for potato number {}".format(potato_number))
            potato_number += 1
        elif key == ord("d"):
            if potato_number > 1:
                potato_number -= 1
            pths = Path(args.output).glob("*_{}_*.bag".format(potato_number))
            for pth in pths:
                os.remove(str(pth))
                print("Deleted Bag for potato number {}".format(potato_number))
        elif key == ord("q"):
            break
        else:
            cam.realtime = False
            display_frame(frames.previous())
