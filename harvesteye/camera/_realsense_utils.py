""" realsense utils functions"""
# pylint: disable=import-error
import pyrealsense2 as rs


def _apply_rs_filters(depth_frame, avg_depth):
    """add fitlers to images"""
    try:
        # Decimation - reduces depth frame density
        thresh = rs.threshold_filter()
        thresh.set_option(rs.option.max_distance, avg_depth)
        dec_filter = rs.decimation_filter()
        dec_filter.set_option(rs.option.filter_magnitude, 1)
        hole_filling = rs.hole_filling_filter()
        spat_filter = rs.spatial_filter()
        temporal = rs.temporal_filter()
        depth_to_disparity = rs.disparity_transform(True)
        disparity_to_depth = rs.disparity_transform(False)
        depth_frame = dec_filter.process(depth_frame)
        """see
        #
        # https://github.com/IntelRealSense/
        ##librealsense/blob/jupyter/notebooks/depth_filters.ipynb
        # https://github.com/IntelRealSense/
        #librealsense/blob/master/doc/post-processing-filters.md
        #spatial-edge-preserving-filter
        # Number of iterations
        #spat_filter.set_option(rs.option.filter_magnitude, 4)
        #spat_filter.set_option(rs.option.filter_smooth_alpha, .1)
        #spat_filter.set_option(rs.option.filter_smooth_delta, 50)
        # 4 is mapped to 16 pixels
        """
        depth_frame = thresh.process(depth_frame)
        spat_filter.set_option(rs.option.holes_fill, 3)
        depth_frame = depth_to_disparity.process(depth_frame)
        depth_frame = spat_filter.process(depth_frame)
        depth_frame = temporal.process(depth_frame)
        depth_frame = disparity_to_depth.process(depth_frame)
        depth_frame = hole_filling.process(depth_frame)
        return depth_frame
    except Exception as error:
        raise error


def _cvtdict2intr(pdict):
    """dict to complex intrinsics object"""
    ret = rs.pyrealsense2.intrinsics()
    ret.width = pdict["width"]
    ret.height = pdict["height"]
    ret.ppx = pdict["ppx"]
    ret.ppy = pdict["ppy"]
    ret.fx = pdict["fx"]
    ret.fy = pdict["fy"]
    ret.model = rs.pyrealsense2.distortion.brown_conrady
    ret.coeffs = pdict["coeffs"]
    return ret


def _cvtintr2dict(intrinsics):
    return {
        "width": intrinsics.width,
        "height": intrinsics.height,
        "ppx": intrinsics.ppx,
        "ppy": intrinsics.ppy,
        "fx": intrinsics.fx,
        "fy": intrinsics.fy,
        "model": intrinsics.model,
        "coeffs": intrinsics.coeffs,
    }
