import sys
from harvesteye.globals._globals import Globals, System, Calibration

from harvesteye.camera import find_device_that_supports_advanced_mode
import pyrealsense2 as rs

gobs = {}
gobs["system"] = System("system")
devs = find_device_that_supports_advanced_mode()
for dev in devs:
    gobs[dev.get_info(rs.camera_info.serial_number)] = Calibration(
        dev.get_info(rs.camera_info.serial_number)
    )


sys.modules[__name__] = gobs
