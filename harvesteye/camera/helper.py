"""Helper module for camera namespace"""
# pylint: disable=superfluous-parens
# pylint: disable=import-error
import cv2
from harvesteye.camera import Realsense
from harvesteye.camera import camera, cameras


def display_frame(frames, name="1"):
    """used to display camera depth and colour"""
    # frames.save("/home/michael/appdata/yolo")
    if frames:
        print("Frame Number: {}".format(frames.frame_number))
        cv2.imshow(name + " rgb", frames.colour_frame)
        cv2.imshow(name + " depth", frames.depth_frame)
        cv2.waitKey(1)


def display_cam(cam):
    while True:
        FRAMES = cam()
        if FRAMES:
            display_frame(FRAMES)


def frame_from_video(cam, frame_number):
    while True:
        frames = cam()
        if frame_number == frames.frame_number:
            return frames


if __name__ == "__main__":
    CAM = camera()
    BAG = camera(bagfile="/home/b-hive/HE004-video.bag")
    CAMS = [CAM, BAG]
    while True:
        [display_frame(cam(), cam.serial) for cam in CAMS]
