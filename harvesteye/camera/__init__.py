"""namespace easy import"""
# pylint: disable=import-error
# pylint: disable=no-name-in-module
from pathlib import Path
from harvesteye.camera._realsense import Realsense, EndOfBagError
from harvesteye.camera.abstracts import DistanceError, Camera
from harvesteye.camera._frames import FrameFromFile
import pyrealsense2 as rs
import sys
import os
import time

from harvesteye import logger

EventLogger = logger.get_logger("EVENT")


class NoCameraFoundError(Exception):
    pass


DS5_product_ids = [
    "0AD1",
    "0AD2",
    "0AD3",
    "0AD4",
    "0AD5",
    "0AF6",
    "0AFE",
    "0AFF",
    "0B00",
    "0B01",
    "0B03",
    "0B07",
    "0B3A",
]


def find_device_that_supports_advanced_mode():
    ctx = rs.context()
    devices = ctx.query_devices()
    print("D: ", devices)
    devs = []
    for dev in devices:
        if (
            dev.supports(rs.camera_info.product_id)
            and str(dev.get_info(rs.camera_info.product_id)) in DS5_product_ids
        ):
            if dev.supports(rs.camera_info.name):
                print(
                    "Found device that supports advanced mode:",
                    dev.get_info(rs.camera_info.name),
                    " -> ",
                    dev,
                )
                devs.append(dev)
    # raise Exception("No device that supports advanced mode was found")
    return devs


def is_streaming(device_serial):
    try:
        c = rs.config()
        c.enable_device(device_serial)
        p = rs.pipeline()
        time.sleep(0.4)
        p.start(c)
        time.sleep(0.4)
        p.stop()
        time.sleep(0.4)
        return False
    except Exception:
        return True


def camera(**kwargs):
    devs = find_device_that_supports_advanced_mode()
    if "bagfile" in kwargs.keys():
        return Realsense(serial=None, **kwargs)
    for dev in devs:
        serial = dev.get_info(rs.camera_info.serial_number)
        if not is_streaming(serial) or serial is None:
            return Realsense(serial=serial, **kwargs)
    raise NoCameraFoundError


def cameras(**kwargs):
    devs = find_device_that_supports_advanced_mode()
    _cams = []
    for dev in devs:
        serial = dev.get_info(rs.camera_info.serial_number)
        if "bagfile" in kwargs.keys():
            serial = None
        if not is_streaming(serial) or serial is None:
            _cams.append(Realsense(serial=serial, **kwargs))
    if len(_cams) > 0:
        return _cams
    raise NoCameraFoundError


# def camera(**kwargs):
#     v4l2path = "/sys/class/video4linux"
#     for base, subs, filenames in os.walk(v4l2path, followlinks=True):
#         for filename in filenames:
#             if filename == "name":
#                 pth = os.path.join(base, filename)
#                 with open(pth, "r") as f:
#                     name = f.read()
#                     for cls in Camera.__subclasses__():
#                         if cls.is_camera(name, kwargs):
#                             return cls(**kwargs)
#     for cls in Camera.__subclasses__():
#         if cls.uses_file(kwargs):
#             return cls(**kwargs)
#         EventLogger.info("[EVENT:NoCameraFound],{'NoCameraFound':1}")
#     raise NoCameraFoundError


# class cameras(list):
#     def __init__(self, folder_path=None, etc=".bag", **kwargs):
#         if folder_path:
#             pths = Path(folder_path).glob("**/*{}".format(etc))
#             for pth in pths:
#                 self.append(camera(bagfile=str(pth), **kwargs))
#
#     def append(self, value):
#         self[len(self) :] = [value]
#
#     def __call__(self, testing=False):
#         ret = []
#         for idx, cam in enumerate(self):
#             attempts = 0
#             while attempts <= 1:
#                 try:
#                     frame = cam()
#                     if frame is None:
#                         raise ValueError
#                 except ValueError:
#                     attempts += 1
#                     cam = camera(**cam.__dict__)
#                 else:
#                     self[idx] = cam
#                     break
#             if frame:
#                 if testing:
#                     frame.potato_number = int(Path(cam.bagfile).stem.split("_")[2])
#                 ret.append(frame)
#         return ret
