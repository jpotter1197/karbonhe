from pathlib import Path
from queue import Queue, Empty
import numpy as np
from abc import ABC, abstractmethod, abstractproperty
from harvesteye.camera.abstracts import DistanceError, Frame
from harvesteye.camera import FrameFromFile
import time
from harvesteye import logger

NSLogger = logger.get_logger("Segmentation")
ErrLogger = logger.get_logger("error")


class Detection(ABC):
    def __init__(self, id=None):
        self.ID = id
        self.dimensions = [None, None, None]
        self.timestamp = time.strftime("%H:%M:%S %Y-%m-%d", time.localtime(time.time()))
        np.random.seed(42)
        self.colors = np.random.randint(0, 255, size=(2, 3), dtype="uint8")
        self.color = None

    @abstractproperty
    def major(self):
        pass

    @abstractproperty
    def minor(self):
        pass

    def __repr__(self):
        return str(self.dimensions)

    def __str__(self):
        if np.isnan(self.dimensions).any():
            return ""
        else:
            line = [x for x in self.dimensions] + [self.timestamp]
            return ", ".join(map(str, line))

    @abstractmethod
    def draw(self):
        pass

    @classmethod
    def is_detection(cls, mtype):
        return mtype.lower() in cls.__name__.lower()

    def near_region(self, region, border=3):
        """
        Not as simple as it sounds, we want to remove potatoes
        that have been chopped off near the border
        To do this the nearest edge to the border could be past
        the border (easy) or just close to it,
        but this could be the case for a whole potatoe that is just near it
        :param box: box to test in cv2 format [point_x,point_y,width,height]
        :param orignal_height: Height of image
        :param orignal_width: Width of image
        :param border: the number of pixels to use as a border
        :return: True if any of the edges are within the border of the image
        """
        # box: [point_x,point_y,width,height]
        if self.bbox[0] < (region[0] + border):
            return True
        if self.bbox[1] < (region[1] + border):
            return True
        if self.bbox[2] > (region[2] - border):
            return True
        if self.bbox[3] > (region[3] - border):
            return True
        return False

    def keep(self, *kwargs):
        return True


class Detections(list, ABC):
    detection = None

    def __init__(self, frame: Frame, **kwargs):
        if not isinstance(frame, Frame):
            raise TypeError
        self.frame = frame
        if not hasattr(self.frame, "regions"):
            shape = self.frame.shape
            self.frame.regions = [[0, 0, shape[1], shape[0]]]

    def append(self, value):
        self[len(self) :] = [value]

    def calculate_dimensions(self):
        NSLogger.debug("Calculating Sizes for %s detections", len(self))
        for detection in self:
            if type(self.frame) is not FrameFromFile:
                length = self.calc_length(detection)
                width = self.calc_width(detection)
                height = self.calc_height(detection)
                try:
                    if length <= 20 or width <= 20:
                        continue
                    if length >= 200 or width >= 200:
                        continue
                    detection.dimensions = (round(length), round(width), round(height))
                    NSLogger.debug(
                        "Successfully Calculated %s size: %s",
                        detection.ID,
                        detection.dimensions,
                    )
                except TypeError:
                    pass
            else:
                detection.dimensions = None
            if not hasattr(self.frame, "regions"):
                shape = self.frame.shape
                self.frame.regions = [[0, 0, shape[1], shape[0]]]
            detection.filtered = any(
                [
                    detection.keep(regions=self.frame.regions),
                    NSLogger.level == 10,
                    isinstance(self.frame, FrameFromFile),
                ]
            )

    def calc_length(self, detection):
        try:
            return self.frame.get_distance(
                *detection.major, center_point=detection.center
            )
        except DistanceError:
            return None

    def calc_width(self, detection):
        try:
            return self.frame.get_distance(
                *detection.minor, center_point=detection.center
            )
        except DistanceError:
            return None

    def calc_height(self, detection):
        width = self.calc_width(detection)
        if width:
            return width * 0.85
        return None

    def draw(self):
        drawn = False
        if NSLogger.level == 10:
            disp = np.array(self.frame.masked)
        else:
            disp = np.array(self.frame.colour_frame)
        startx = time.time()
        for obj in self:
            if obj.filtered:
                disp = obj.draw(disp)
                drawn = True
        NSLogger.info(
            "Detections: Took %s Drawing %s Objects", time.time() - startx, len(self)
        )
        if drawn:
            return disp
        else:
            return None

    def __str__(self):
        lines = []
        for obj in self:
            if obj.filtered:
                lines.append(str(obj))
        return ";".join(lines)


class Segmenter(ABC):
    _defaults = {
        "confidence_thresh": 0.3,
        "threshold_thresh": 0.8,
        "inference_res": 832,
        "object_queue": Queue(),
    }
    detections = Detections

    def __init__(self, path, **kwargs):
        try:
            NSLogger.debug("Initialising %s", (path, self.detections, kwargs))
            self.__dict__.update(self._defaults)
            self.path = Path(path).glob("**/*")
            files_in_folder = [x for x in self.path if x.is_file()]
            files = {}
            for file in files_in_folder:
                files[file.suffix[1:]] = file
            self.__dict__.update(files)
            self.__dict__.update(kwargs)
            self.setup()
        except Exception as error:
            NSLogger.exception("%s Errored during initialising", self.__dict__)
            ErrLogger.error("Errored during initialising")
            raise error

    def __call__(self, timeout=None):
        try:
            ret = self.object_queue.get(timeout=timeout)
            NSLogger.debug("queue returned %s detections", len(ret))
            return ret
        except Empty:
            return None

    @abstractmethod
    def setup(self):
        pass

    @abstractmethod
    def inference(self, frame, detection_type, display=None):
        pass

    @classmethod
    @abstractmethod
    def is_segmenter(cls, model_file):
        pass

    @abstractmethod
    def filter(self, objs):
        pass
