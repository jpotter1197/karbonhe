from harvesteye.server.abstracts import RequestHandlerAbstract
import xml.etree.ElementTree as ET

from harvesteye import logger

NSLogger = logger.get_logger("Server")
ErrLogger = logger.get_logger("error")


class RequestHandler(RequestHandlerAbstract):
    def handle(self):
        self._status = True
        self.start()
        while self._status:
            data = self.message
            try:
                if data is None:
                    continue
                elif data == "request":
                    self.send_message(getattr(self, "Request")("Request"))
                elif data[0] == "<":
                    data = ET.fromstring(data)
                    self.send_message(getattr(self, data.tag)(data))
                else:
                    self.send_message(getattr(self, data)(data))
            except TypeError as error:
                if "object is not callable" in str(error):
                    NSLogger.error("No Handle called %s", data)
                else:
                    NSLogger.exception("Errored while handling message", self.__dict__)
                    ErrLogger.error("Errored while handling message")


# class RequestHandlerNew(RequestHandler):
#     def handle(self):
#         message_length = self.convert_size_from_byte(self.request.recv(4))
#         data = self._receive(message_length).decode()
#         NSLogger.info("{} wrote: {}".format(self.client_address[0], data))
#         if data[0] == '<':
#             data = ET.fromstring(data)
#             self.send_message(getattr(self, data.tag)(data))
#         else:
#             self.send_message(getattr(self, data)(data))
