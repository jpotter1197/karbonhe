"""software.py:
This class is a version controller for software installed with updaters"""
import os
import pickle


def get_list_of_files(dir_name, etx=".txt", sub=False):
    """
    create a list of file and sub directories
    names in the given directory
    """
    list_of_file = os.listdir(dir_name)
    all_files = list()
    # Iterate over all the entries
    for entry in list_of_file:
        # Create full path
        full_path = os.path.join(dir_name, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(full_path) and sub:
            all_files = all_files + get_list_of_files(full_path, etx, sub)
        else:
            if etx in full_path:
                all_files.append(full_path)
    return all_files


class Software:
    """Software Object"""

    def __init__(self, name, version, restore=0):
        self.name = name
        self._version = version
        self._rf = restore

    @property
    def version(self):
        """Version of software getter"""
        return self._version

    @version.setter
    def version(self, value):
        """Version of software setter"""
        self._version = value

    def is_software(self, name):
        """Returns true if name is the software"""
        return name == self.name

    def version_match(self, version):
        """Check if version match"""
        return version in (self.version, self._rf)


class Softwares:
    """List of Software"""

    def __init__(self, base_path=os.path.abspath(os.path.dirname(__file__))):
        self.path = os.path.join(base_path, "SoftwareUpdater/")
        self.patchpath = os.path.join(self.path, "patches/")
        self.logpath = os.path.join(self.path, "log/")
        self.softwareinfo = os.path.join(self.path, "softwareinfo/")
        dirs = [self.path, self.patchpath, self.logpath, self.softwareinfo]
        for adir in dirs:
            if not os.path.exists(adir):
                os.mkdir(adir)
        self._li = self.load_software()
        # atexit.register(self.save)
        # print("Software Loaded")

    def save(self, software):
        """pickle dumpes software objects"""
        pkl = software.name + ".pkl"
        with open(os.path.join(self.softwareinfo, pkl), "wb") as file:
            pickle.dump(software, file)
        # print("saved {}".format(software.name))

    def is_installed(self, name):
        """checks if the software is installed, returns bool"""
        bools = []
        for software in self._li:
            bools.append(software.is_software(name))
        return any(bools)

    def load_software(self):
        """Loads in software from memory"""
        _li = []
        pkllist = get_list_of_files(self.softwareinfo, ".pkl")
        for pkl in pkllist:
            with open(pkl, "rb") as file:
                software = pickle.load(file)
            _li.append(software)
        self._li = _li
        return _li

    def create_software(self, name, version, restore=0):
        """creates new pickle object in memory"""
        software = Software(name, version, restore)
        self.install_software(software)

    def install_software(self, software):
        """save given software"""
        if self.is_installed(software.name):
            return
        self._li.append(software)

    def uninstall_software(self, name):
        """uninstalls software by name"""
        if self.is_installed(name):
            uninstalled_software = self._li[self.idx_of(name)]
            self._li.remove(uninstalled_software)
            if os.path.exists(os.path.join(self.softwareinfo, name + ".pkl")):
                os.remove(os.path.join(self.softwareinfo, name + ".pkl"))

    def __getitem__(self, item):
        """gettings for list"""
        return self._li[item]

    def idx_of(self, name):
        """Find idx of software"""
        return [i for i, x in enumerate(self._li) if x.name == name][0]


if __name__ == "__main__":
    SOFTWARES = Softwares("/mnt/data/")
